package com.digitalbotanist.javaPlots;

import java.awt.Graphics2D;
import java.awt.geom.Line2D;

import com.digitalbotanist.javaPlots.Axis;
import com.digitalbotanist.javaPlots.BasePlot;
import com.digitalbotanist.javaPlots.OutOfRangeException;


abstract class Abcissa extends Axis{
	
	Abcissa(BasePlot parent) {
		super(parent);
	}
	
	public void draw(Graphics2D g2){
		int padding = parent.getPadding();
		int yPadding = padding + getCrossing();
		int h = parent.getHeight();
		int w = parent.getWidth();
		
		//draw the line and values
		g2.draw(new Line2D.Double(padding, h-yPadding, w-padding, h-yPadding));
		//draw the values
		g2.drawString(Double.toString(maxValue), w-padding, h-padding + 12);
		g2.drawString(Double.toString(minValue), 2, h-padding+12);
	}
	
	abstract public int getScaledXVal(double yVal) throws OutOfRangeException;
}
