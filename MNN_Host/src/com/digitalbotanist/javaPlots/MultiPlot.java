package com.digitalbotanist.javaPlots;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import javax.swing.JPanel;

import com.digitalbotanist.javaPlots.BasePlot;
import com.digitalbotanist.javaPlots.FinePoint;
import com.digitalbotanist.javaPlots.TimePlot;


public class MultiPlot extends JPanel{
	HashMap<String, BasePlot> plots = new HashMap<String, BasePlot>();


	
	public void addPlot(BasePlot b, String name){
		System.out.println("Multiplot adding new plot:" + name);
		plots.put(name, b);
	}
	
	public void addTimePlot(TimePlot b, String name){
		System.out.println("Multiplot adding new timeplot:" + name);
		plots.put(name, b);
		this.addMouseMotionListener(b);
		b.setDisplay(this);
	}
	
	public void removePlot(String name){
		plots.remove(name);
	}
	
	public void addPoint(FinePoint p, String plotName){
		//System.out.println("Adding MultiPlot Point: "  + p.x + "," + p.y + " to plot: " + plotName);
		BasePlot plot = plots.get(plotName);
		plot.addPoint(p);
		setScales();
		this.repaint();
	}
	
	private void setScales() {
		//find the max and min axis values encompassing all visible plots
		//BasePlot firstPlot = plots.values().iterator().next();
		Double maxX = Double.MIN_VALUE;//firstPlot.getXMax();
		Double maxY = Double.MIN_VALUE;//firstPlot.getYMax();
		Double minX = Double.MAX_VALUE;//firstPlot.getXMin();
		Double minY = Double.MAX_VALUE;//firstPlot.getYMin();
		for(BasePlot p: plots.values()){
			if(p.isVisible()){
				maxX = Math.max(maxX, p.getXMax());
				maxY = Math.max(maxY, p.getYMax());
				minX = Math.min(minX, p.getXMin());
				minY = Math.min(minY, p.getYMin());
			}
		}
		
		for(BasePlot p: plots.values()){
			if(p.isVisible()){
				p.setXScale(minX, maxX);
				p.setYScale(minY, maxY);
			}
		}		
	}

	public void paintComponent(Graphics g) {
		//System.out.println("Multiplot Paint");
		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D)g;
		Set<String> names = plots.keySet();
		for(String name : names){
			BasePlot p = plots.get(name);
			if(p.isVisible()){
				p.setSize(this.getWidth(), this.getHeight());
		        p.drawOrdinate(g2);
		        p.drawAbcissa(g2);
				p.drawPoints(g2);	
			}
		}
	}
	
	public void setPlotVisibiltiy(String plotName, boolean visible){
		plots.get(plotName).setVisible(visible);
		setScales();
		this.repaint();
	}
	
	public void setPlotColor(String plotName, Color c){
		BasePlot p = plots.get(plotName);
		if(p!=null){
			p.setDotColor(c);		
			this.repaint();
		}else{
			System.out.println("Multiplot - setPlotColor: could not find plot: " + plotName);
		}
	}

	public void clearPlotPoints(String plotName){
		BasePlot p = plots.get(plotName);
		if(p!=null){
			p.clearPoints();
			setScales();
			this.repaint();
		}
	}
	
	public boolean hasPlot(String name) {
		return plots.containsKey(name);
	}
	
}
