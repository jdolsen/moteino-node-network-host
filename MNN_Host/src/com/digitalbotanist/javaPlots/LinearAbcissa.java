package com.digitalbotanist.javaPlots;

import com.digitalbotanist.javaPlots.Abcissa;
import com.digitalbotanist.javaPlots.BasePlot;
import com.digitalbotanist.javaPlots.OutOfRangeException;

public class LinearAbcissa extends Abcissa {

	LinearAbcissa(BasePlot parent) {
		super(parent);
	}

	@Override
	public int getScaledXVal(double xVal) throws OutOfRangeException {
		if(xVal < minValue || xVal > maxValue){
			throw new OutOfRangeException();
		}else{
			int axisL = parent.getWidth() - 2*parent.getPadding();
			double axisRange = maxValue-minValue;
			double rangePercent = (xVal - minValue)/axisRange;
			return (int)(axisL*rangePercent);
		}
	}
}
