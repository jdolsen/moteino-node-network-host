package com.digitalbotanist.javaPlots;

import com.digitalbotanist.javaPlots.BasePlot;
import com.digitalbotanist.javaPlots.Ordinate;
import com.digitalbotanist.javaPlots.OutOfRangeException;

public class LinearOrdinate extends Ordinate {
	
	public LinearOrdinate(BasePlot parent) {
		super(parent);
	}

	@Override
	public int getScaledYVal(double yVal) throws OutOfRangeException {
		if(yVal < minValue || yVal > maxValue){
			throw new OutOfRangeException();
		}else{
			int axisH = parent.getHeight() - 2*parent.getPadding();
			double axisRange = maxValue-minValue;
			double rangePercent = (yVal - minValue)/axisRange;
			return (int)(axisH*rangePercent);
		}
	}
}
