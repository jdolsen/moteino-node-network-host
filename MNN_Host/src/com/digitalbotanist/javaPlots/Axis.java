package com.digitalbotanist.javaPlots;

import com.digitalbotanist.javaPlots.BasePlot;

public abstract class Axis {
	double minValue;
	double maxValue;
	int crossing = 0;
	boolean showValues = true;
	protected BasePlot parent;
	
	Axis(BasePlot p){
		parent = p;
	}
	
	public void setCrossing(int c){
		this.crossing = c;
	}
	
	public int getCrossing(){
		return crossing;
	}
	
	public void setRange(double min, double max){
		this.minValue = min;
		this.maxValue = max;
	}
	
	public double getMinValue() {
		return minValue;
	}

	public void setMinValue(double minValue) {
		this.minValue = minValue;
	}

	public double getMaxValue() {
		return maxValue;
	}

	public void setMaxValue(double maxValue) {
		this.maxValue = maxValue;
	}

	public boolean isShowValues() {
		return showValues;
	}

	public void setShowValues(boolean showValues) {
		this.showValues = showValues;
	}
	
}
