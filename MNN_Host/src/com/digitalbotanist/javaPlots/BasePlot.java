package com.digitalbotanist.javaPlots;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.JPanel;

import com.digitalbotanist.javaPlots.Abcissa;
import com.digitalbotanist.javaPlots.FinePoint;
import com.digitalbotanist.javaPlots.LinearAbcissa;
import com.digitalbotanist.javaPlots.LinearOrdinate;
import com.digitalbotanist.javaPlots.Ordinate;

public abstract class BasePlot extends JPanel{





	private static final long serialVersionUID = 1L;
    int padding = 35;   
    boolean hasLineConnectors;
    boolean isYAutoScale;
    boolean isXAutoScale;
    double yScaleMin;
    double yScaleMax;
    double xScaleMin;
    double xScaleMax;
    Color dotColor = Color.red;
    protected boolean visible = true;
    protected String label;
    protected FinePoint labelLocation;
    
    protected List<FinePoint> graphPoints = Collections.synchronizedList(new ArrayList<FinePoint>());
    protected Ordinate ordinate = new LinearOrdinate(this);
    protected Abcissa abcissa = new LinearAbcissa(this);
    
    BasePlot(Color c){
    	this.dotColor = c;
    }
    
    BasePlot(){
    }
    
    abstract void drawPoints(Graphics2D g2);
    
    public void addPoint(FinePoint p){
    	//System.out.println("Adding base finepoint");
    	graphPoints.add(p);
    	this.repaint();
    }
    
    public void addPoint(double x, double y){
    	FinePoint p = new FinePoint(x,y);
    	addPoint(p);
    }

	void drawOrdinate(Graphics2D g2){
    	if(isYAutoScale){
    		ordinate.setRange(getYMin(), getYMax());
    	}else{
    		ordinate.setRange(yScaleMin, yScaleMax);
    	}
    	ordinate.draw(g2);	
    }
	
	void drawAbcissa(Graphics2D g2){
		if(isXAutoScale){
			abcissa.setRange(getXMin(), getXMax());
		}else{
			abcissa.setRange(xScaleMin, xScaleMax);
		}
		abcissa.draw(g2);
	}
    
    public void setYScale(double min, double max){
    	this.yScaleMin = min;
    	this.yScaleMax = max;
    	ordinate.setRange(min, max);
    }
    
    public void setXScale(double min, double max){
    	this.xScaleMin = min;
    	this.xScaleMax = max;
    	abcissa.setRange(min, max);
    }
    
    public double getYMax() {
        double max = -Double.MAX_VALUE;
        for(int i = 0; i < graphPoints.size(); i++) {
            if(graphPoints.get(i).getY() > max)
                max = graphPoints.get(i).getY();
        }
        return max;
    }
    
    public double getYMin() {
        double min = Double.MAX_VALUE;
        for(int i = 0; i < graphPoints.size(); i++) {
            if(graphPoints.get(i).getY() < min)
                min = graphPoints.get(i).getY();
        }
        return min;
    }
    
    public double getXMax() {
        double max = -Double.MAX_VALUE;
        for(int i = 0; i < graphPoints.size(); i++) {
            if(graphPoints.get(i).getX() > max)
                max = graphPoints.get(i).getX();
        }
        return max;
    }
    
    public double getXMin() {
        double min = Double.MAX_VALUE;
        for(int i = 0; i < graphPoints.size(); i++) {
            if(graphPoints.get(i).getX() < min)
            	min = graphPoints.get(i).getX();
        }
        return min;
    }
    
    void clear(Graphics2D g2){
    	int w = getWidth();
    	int h = getHeight();
    	g2.clearRect(0, 0, w, h);
    }
    
    public void clearPoints(){
    	graphPoints.clear();

    	//this.repaint();
    }
    
    public boolean isYAutoScale() {
		return isYAutoScale;
	}

	public void setYAutoScale(boolean isYAutoScale) {
		this.isYAutoScale = isYAutoScale;
	}

	public boolean isXAutoScale() {
		return isXAutoScale;
	}

	public void setXAutoScale(boolean isXAutoScale) {
		this.isXAutoScale = isXAutoScale;
	}

	public double getyScaleMin() {
		return yScaleMin;
	}

	public void setyScaleMin(double yScaleMin) {
		this.yScaleMin = yScaleMin;
	}

	public double getyScaleMax() {
		return yScaleMax;
	}

	public void setyScaleMax(double yScaleMax) {
		this.yScaleMax = yScaleMax;
	}

	public Ordinate getOrdinate() {
		return ordinate;
	}

	public void setOrdinate(Ordinate ordinate) {
		this.ordinate = ordinate;
	}

	public double getxScaleMin() {
		return xScaleMin;
	}

	public void setxScaleMin(double xScaleMin) {
		this.xScaleMin = xScaleMin;
	}

	public double getxScaleMax() {
		return xScaleMax;
	}

	public void setxScaleMax(double xScaleMax) {
		this.xScaleMax = xScaleMax;
	}

	public Abcissa getAbcissa() {
		return abcissa;
	}

	public void setAbcissa(Abcissa abcissa) {
		this.abcissa = abcissa;
	}
	
	public int getPadding() {
		return padding;
	}

	public void setPadding(int padding) {
		this.padding = padding;
	}

	public boolean isHasLineConnectors() {
		return hasLineConnectors;
	}

	public void setHasLineConnectors(boolean hasLineConnectors) {
		this.hasLineConnectors = hasLineConnectors;
	}

	public Color getDotColor() {
		return dotColor;
	}

	public void setDotColor(Color dotColor) {
		this.dotColor = dotColor;
	}

	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;		
	}
	
	public void setLabel(String text, FinePoint location){
		this.label = text;
		this.labelLocation = location;
	}
}
