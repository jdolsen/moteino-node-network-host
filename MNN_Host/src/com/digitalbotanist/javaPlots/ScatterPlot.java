package com.digitalbotanist.javaPlots;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.Ellipse2D;

import javax.swing.JFrame;

import com.digitalbotanist.javaPlots.BasePlot;
import com.digitalbotanist.javaPlots.FinePoint;
import com.digitalbotanist.javaPlots.OutOfRangeException;
import com.digitalbotanist.javaPlots.ScatterPlot;


public class ScatterPlot extends BasePlot {

	
	public ScatterPlot(Color c){
		super(c);
		setXAutoScale(true);
		setYAutoScale(true);
		setYScale(-1000, 1000);
		setXScale(0, 100);	
	}

	@Override
	void drawPoints(Graphics2D g2) {
	   	int w = getWidth();
    	int h = getHeight();
        //double xInc = (double)(w - 2*padding)/(graphPoints.size());
        
        // Mark data points.
        FinePoint prev = null;
        for(int i = 0; i < graphPoints.size(); i++){
        	try {
        		FinePoint current = graphPoints.get(i);
	            double x = padding+abcissa.getScaledXVal(current.getX());
	            double y = h-padding-ordinate.getScaledYVal(current.getY());
	            g2.setPaint(this.dotColor);
	            g2.fill(new Ellipse2D.Double(x-2, y-2, 4, 4));
	            if(prev == null){
	            	prev = new FinePoint(current);
	            }else{
	            	g2.setPaint(Color.black);
	            	g2.drawLine((int)(padding+abcissa.getScaledXVal(prev.getX())), (int)(h - padding - ordinate.getScaledYVal(prev.getY())), (int)x, (int)y);
	            	prev = current;
	            }
			} catch (OutOfRangeException e) {
				prev = null;
			} catch (Exception e){
				System.out.println("Caught Exception");
			}
        }    	
	}
	
    public void paintComponent(Graphics g) {
        super.paintComponent(g);  
        Graphics2D g2 = (Graphics2D)g;        
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                            RenderingHints.VALUE_ANTIALIAS_ON);        
        clear(g2);
		
		try {
			ordinate.setCrossing(abcissa.getScaledXVal(0));
		} catch (OutOfRangeException e) {
			try {
				ordinate.setCrossing(abcissa.getScaledXVal(abcissa.getMinValue()));
			} catch (OutOfRangeException e1) {
				// TODO Auto-generated catch block
				//e1.printStackTrace();
			}
			//e.printStackTrace();
		}
		
		try {
			abcissa.setCrossing(ordinate.getScaledYVal(0));
		} catch (OutOfRangeException e) {
			try {
				abcissa.setCrossing(ordinate.getScaledYVal(ordinate.getMinValue()));
			} catch (OutOfRangeException e1) {
				// TODO Auto-generated catch block
				//e1.printStackTrace();
			}
			//e.printStackTrace();
		}
		
        drawOrdinate(g2);
        drawAbcissa(g2);
        drawPoints(g2);    
    }
    
    public static void main(String[] args) {
        JFrame f = new JFrame();
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setSize(600,400);
        final ScatterPlot graph1 = new ScatterPlot(Color.green);

        
        f.add(graph1);


        f.setLocation(600,200);
        f.setVisible(true);
    }

}
