package com.digitalbotanist.javaPlots;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.digitalbotanist.javaPlots.FinePoint;

public class GraphInfo {
	private List<FinePoint> graphPoints = Collections.synchronizedList(new ArrayList<FinePoint>());
	private String name;
	private String label;
	private Color defaultColor = Color.red;
	private boolean displayConnectors = true;
	
	GraphInfo(String n, String l, Color c){
		name=n;
		label=l;
		defaultColor=c;
	}
	
    public void addPoint(FinePoint p){
    	if(p.color == null){
    		p.color = defaultColor;
    	}
    	graphPoints.add(p);
    }
    
    public void removePoint(int index){
    	graphPoints.remove(index);
    }
    
    public void drawPoints(int xmin, int xmax, int ymin, int ymax, 
    					   double xMinVal, double xMaxVal, double yMinVal, double yMaxVal,
    					   Graphics2D g2)
    {
    	//currently, this assumes a linear plot.
    	double xRatio = ((double)(xmax-xmin))/(xMaxVal-xMinVal);
    	double yRatio = ((double)(ymax-ymin))/(yMaxVal-yMinVal);
    	if(graphPoints.size()>0){
	    	FinePoint previous = graphPoints.get(0);
	    	for(FinePoint p: graphPoints){
	    		if(p.pointIsInRange(xMinVal, xMaxVal, yMinVal, yMaxVal)){
	    			int dispX = (int) (xmin + (xRatio * p.getX()));
	    			int dispY = (int) (ymin + (yRatio * p.getY()));
			
	    			g2.setPaint(defaultColor);
	 	            g2.fill(new Ellipse2D.Double(dispX-2, dispY-2, 4, 4));
	 	            
	 	            if(displayConnectors){
		    			int prevX = (int) (xmin + (xRatio * previous.getX()));
		    			int prevY = (int) (ymin + (yRatio * previous.getY()));	
	 	            	g2.setPaint(Color.black);
		            	g2.drawLine(prevX, prevY, dispX, dispY);
		            	previous = p;
	 	            }
	 	            
	    		}else{
	    			//do not display the point, it is out of range
	    		}
	    	}
    	}
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public Color getDefaultColor() {
		return defaultColor;
	}

	public void setDefaultColor(Color defaultColor) {
		this.defaultColor = defaultColor;
	}

	public boolean isDisplayConnectors() {
		return displayConnectors;
	}

	public void setDisplayConnectors(boolean displayConnectors) {
		this.displayConnectors = displayConnectors;
	}
    
   
}
