package com.digitalbotanist.javaPlots;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Ellipse2D.Double;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JFrame;
import javax.swing.JPanel;

import com.digitalbotanist.javaPlots.BasePlot;
import com.digitalbotanist.javaPlots.FinePoint;
import com.digitalbotanist.javaPlots.OutOfRangeException;
 
public class TimePlot extends BasePlot  implements MouseMotionListener {
		
	//private int numPoints = 200;
	private int displaySeconds = 10;
	Timer timer = new Timer();
	JPanel display = null;
	
	public TimePlot(int displaySeconds){
		this(displaySeconds,Color.red);
	}
	
	public TimePlot(int displaySeconds, Color c){
		super(c);
		this.setYAutoScale(true);
		setXScale(0,displaySeconds);
		this.setXAutoScale(false);
		this.displaySeconds = displaySeconds;
		      
        timer.scheduleAtFixedRate(new TimerTask(){
        	public void run(){
        		removeOldPoints(Calendar.getInstance().getTimeInMillis());
        		if(display!=null){
        			display.repaint();
        		}else{
        			repaint();
        		}
        	}
        }, 1000, 87);    
	}
	
	public double getXMax(){
		return displaySeconds;
	}
	
	public double getXMin(){
		return 0;
	}
    
    public void addPoint(FinePoint y){ 
    	//long timeMs = Calendar.getInstance().getTimeInMillis(); 
    	//FinePoint fpNew = new FinePoint(timeMs,y.getY());
    	super.addPoint(y);   	
    }
    
    public void addPoint(java.lang.Double pt){
    	long timeMs = Calendar.getInstance().getTimeInMillis();
    	FinePoint fpNew = new FinePoint(timeMs,pt.doubleValue());
    	super.addPoint(fpNew);
    }
    
    private void removeOldPoints(long curTimeMs){
    	for(int i = 0; i < graphPoints.size(); ++i){
    		FinePoint first = graphPoints.get(0);
    		//if the point is null or the point occurred within the last "displaySeconds" time, break
    		if(first==null || (curTimeMs - first.x) <= (displaySeconds * 1000)){
    			break;
    		}else{//remove the old point
    			graphPoints.remove(0);
    		}
    	}
    }
    
	@Override
	public void mouseMoved(MouseEvent e) {
		if(isVisible()){
			Point mousePoint = e.getPoint();
			
			//iterate over the points to see if one is hovered over;
			boolean pointHoveredOver = false; //prevents multiple points displaying hover status
			for(FinePoint f: graphPoints){			
	        	Point p = getScaledPoint(f);
	        	double x = p.getX();         
				double y = p.getY();
				double mouseX = mousePoint.getX();
				double mouseY = mousePoint.getY();
				double margin = 2;
				
				boolean previousHover = f.isHoveredOver;
				if(mouseX > x-margin && mouseX < x+margin && 
				   mouseY > y-margin && mouseY < y+margin &&
				   !pointHoveredOver){
					f.isHoveredOver = true;
					pointHoveredOver = true;
				}else{
					f.isHoveredOver = false;
				}
				
				if(f.isHoveredOver!=previousHover){
					display.repaint();
				}
			}
		}
	}
	
	@Override
	public void mouseDragged(MouseEvent e) {
		// Do nothing		
	}
    
    void drawPoints(Graphics2D g2){        
        // Mark data points.
        FinePoint prev = null;
        FinePoint hoverPoint = null;
        for(int i = 0; i < graphPoints.size(); i++){
        	//draw the point
    		FinePoint current = graphPoints.get(i);
    		Point p = getScaledPoint(current);
    		if(p!=null){
	            g2.setPaint(dotColor);
	            g2.fill(new Ellipse2D.Double(p.getX()-2, p.getY()-2, 4, 4));
	
	            //draw the line
	            if(prev == null){
	            	prev = new FinePoint(current);
	            }else{	 
	            	Point prevP = getScaledPoint(prev);      	
	            	g2.drawLine(prevP.x, prevP.y, p.x, p.y);
	            	prev = current;
	            }
	            
	        	//highlight the point and draw the tooltip if necessary
	            if(current.isHoveredOver){   
	            	hoverPoint = current;
	            }          	            
    		}    		
        }//end for 
        
        if(hoverPoint != null){
        	Point p = getScaledPoint(hoverPoint);
        	g2.drawOval(p.x-4, p.y-4, 8, 8);  
        	drawTooltip(g2, hoverPoint, getPointInfoText(hoverPoint));
        	//TODO:draw the point's label as well
        }
        g2.setPaint(Color.black);
        
        if(this.label != null){
        	drawTooltip(g2, labelLocation, label);
        }
    }
       
    private void drawTooltip(Graphics2D g2, FinePoint current, String text) {
		Point p = getScaledPoint(current);
        double x = p.getX();            
        double y = p.getY(); 
        
    	g2.setPaint(Color.black);            	
    	//String infoString = java.lang.Double.toString(current.y) + " at " + getTimeString(current.x);
    	int length = g2.getFontMetrics().stringWidth(text);
    	int yOffset = g2.getFontMetrics().getHeight();
    	int xOffset = 0;
    	int width = getWidth();
    	if(x+length > width){
    		xOffset = (int) (width-(x+length));
    	}
    	g2.setPaint(Color.LIGHT_GRAY.brighter());
    	g2.fillRect((int)x+xOffset, (int)y-(yOffset)-8, length+4, yOffset+4);

    	g2.setPaint(Color.black);
    	g2.drawString(text, (int)x+xOffset, (int)y-(yOffset/2));
	}
    
    private String getPointInfoText(FinePoint current){
    	return java.lang.Double.toString(current.y) + " at " + getTimeString(current.x);
    }
    

	private String getTimeString(double value){
    	Calendar cal = Calendar.getInstance();
    	cal.setTimeInMillis((long) value);
    	int hour = cal.get(Calendar.HOUR_OF_DAY);
    	String minutes = String.format("%02d",cal.get(Calendar.MINUTE));
    	String seconds = String.format("%02d",cal.get(Calendar.SECOND));
    	return hour+":"+minutes+":"+seconds;
    }
    
    private int getScaledXVal(double timeMs, int xWidth){
    	long timeCur = Calendar.getInstance().getTimeInMillis();
    	long timeOldMs = timeCur - (displaySeconds*1000);
    	double window = (displaySeconds*1000);
    	double timePoint = (double) (timeMs-timeOldMs);
    	int x = (int) (xWidth * (timePoint/window));   	
    	return x;
    }
    
    public void paintComponent(Graphics g) {
        super.paintComponent(g);  
        Graphics2D g2 = (Graphics2D)g;        
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                            RenderingHints.VALUE_ANTIALIAS_ON);        
        clear(g2);
        drawOrdinate(g2);
        drawAbcissa(g2);
        drawPoints(g2);    
    }
    
    public Point getScaledPoint(FinePoint f){   	         
		try {
			int w = getWidth();
	    	int h = getHeight();
			int xWidth = (w - 2*padding);
	    	double x = padding + getScaledXVal((long) f.getX(), xWidth);//i*xInc;   
			double y = h-padding-ordinate.getScaledYVal(f.getY());
			Point p = new Point();
			p.setLocation(x, y);
			return p;
		} catch (OutOfRangeException e) {
			e.printStackTrace();
			return null;
		}		
    }
    
    public void setDisplay(JPanel d){
    	this.display = d;
    }
}