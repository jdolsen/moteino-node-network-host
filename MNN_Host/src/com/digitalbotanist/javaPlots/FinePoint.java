package com.digitalbotanist.javaPlots;

import java.awt.Color;

public class FinePoint {
	public double x;
	public double y;
	public Color color = Color.red;
	public boolean isHoveredOver = false;
	String label = null;
	
	public FinePoint(double x, double y, Color c){
		setLocation(x,y);
		color = c;
	}
	
	public FinePoint(double x, double y){
		setLocation(x,y);
	}
	
	FinePoint(FinePoint fp){
		setLocation(fp.x, fp.y);
		this.color = fp.color;
	}
	
	public void setLocation(double x, double y){
		this.x = x;
		this.y = y;
	}
	
	public double getX(){
		return x;
	}
	
	public void setX(double x){
		this.x = x;
	}	
	
	public double getY(){
		return y;
	}
	
	public void setY(double y){
		this.y = y;
	}
	
	public boolean pointIsInRange(double xMinVal, double xMaxVal, double yMinVal, double yMaxVal){
		 if(x<=xMaxVal &&
			x>=xMinVal &&
			y<=yMaxVal &&
			y>=yMinVal){
			 return true;
		 }else{
			 return false;
		 }
	}
	
	public void setIsHoveredOver(boolean val){
		isHoveredOver = val;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
}
