package com.digitalbotanist.javaPlots;
import java.awt.Graphics2D;
import java.awt.geom.Line2D;

import com.digitalbotanist.javaPlots.Axis;
import com.digitalbotanist.javaPlots.BasePlot;
import com.digitalbotanist.javaPlots.OutOfRangeException;


abstract class Ordinate extends Axis{

	boolean isLeftHand = true;
	
	public Ordinate(BasePlot parent){
		super(parent);
	}
	
	public void draw(Graphics2D g2){
		int padding = parent.getPadding();
		int xPadding = padding + getCrossing();
		int h = parent.getHeight();
		int w = parent.getWidth();
		
		//draw the line and values
		if(isLeftHand){
			int yVal = h-padding;

			g2.draw(new Line2D.Double(xPadding, padding, xPadding, h-padding));
			//draw the values
			g2.drawString(Double.toString(maxValue), 2, padding);
			g2.drawString(Double.toString(minValue), 2, h-padding);
		}else{
			g2.draw(new Line2D.Double(w-padding, padding, w-padding, h-padding));
		}
	}
	
	abstract public int getScaledYVal(double yVal) throws OutOfRangeException;
	
	public boolean isLeftHand() {
		return isLeftHand;
	}

	public void setLeftHand(boolean isLeftHand) {
		this.isLeftHand = isLeftHand;
	}

}
