package com.digitalbotanist.mnnHost;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;

import javax.net.ssl.HttpsURLConnection;
//import com.google.gson.Gson;
//import com.google.gson.GsonBuilder;

public class ThingSpeakPostAction {
	
	private static final String APIURL = "https://api.thingspeak.com/update";
	private static final String APIKEYHEADER = "X-THINGSPEAKAPIKEY";
	private String WRITE_API_KEY;
	
	public ThingSpeakPostAction(String apiKey){
		WRITE_API_KEY = apiKey;		
	}
	
	public void post(String data) {
		System.out.println("Writing to thingspeak: " + data);
		try {
			URL url = new URL(APIURL);
			HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
			
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Connection", "close");
			conn.setRequestProperty(APIKEYHEADER, WRITE_API_KEY);
			conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			conn.setRequestProperty("Content-Length", Integer.toString(data.length()));
			
			conn.setDoOutput(true);
			
			DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
			wr.writeBytes(data);
			wr.flush();
			wr.close();

			int responseCode = conn.getResponseCode();
			
			BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			
			//print result
			System.out.println(response.toString());
			
			
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
