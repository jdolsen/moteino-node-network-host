package com.digitalbotanist.mnnHost.sandbox;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.border.TitledBorder;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import javax.swing.JCheckBox;
import javax.swing.UIManager;
import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import javax.swing.JList;
import javax.swing.ListSelectionModel;
import javax.swing.AbstractListModel;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTextPane;
import java.awt.TextArea;
import java.awt.Button;
import java.awt.Dimension;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JMenu;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.JTabbedPane;


public class MyAppWindow {

	private JFrame frmSensorNetwork;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MyAppWindow window = new MyAppWindow();
					window.frmSensorNetwork.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MyAppWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmSensorNetwork = new JFrame();
		frmSensorNetwork.setTitle("Moteino Node Network");
		frmSensorNetwork.setBounds(100, 100, 1135, 742);
		frmSensorNetwork.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] {762, 259, 0};
		gridBagLayout.rowHeights = new int[]{320, 168, 0};
		gridBagLayout.columnWeights = new double[]{1.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{1.0, 0.0, Double.MIN_VALUE};
		frmSensorNetwork.getContentPane().setLayout(gridBagLayout);
		
		JPanel graphPanel = new JPanel();
		GridBagConstraints gbc_graphPanel = new GridBagConstraints();
		gbc_graphPanel.insets = new Insets(0, 0, 5, 5);
		gbc_graphPanel.fill = GridBagConstraints.BOTH;
		gbc_graphPanel.gridx = 0;
		gbc_graphPanel.gridy = 0;
		frmSensorNetwork.getContentPane().add(graphPanel, gbc_graphPanel);
		
		//status panel
		JPanel statusPanel = new JPanel();
		statusPanel.setBorder(new TitledBorder(null, "Status", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagConstraints gbc_statusPanel = new GridBagConstraints();
		gbc_statusPanel.insets = new Insets(0, 0, 5, 0);
		gbc_statusPanel.fill = GridBagConstraints.BOTH;
		gbc_statusPanel.gridx = 1;
		gbc_statusPanel.gridy = 0;
		frmSensorNetwork.getContentPane().add(statusPanel, gbc_statusPanel);
		GridBagLayout gbl_statusPanel = new GridBagLayout();
		gbl_statusPanel.columnWidths = new int[]{1, 62, 142, 33, 0};
		gbl_statusPanel.rowHeights = new int[]{1, 20, 21, 21, 0};
		gbl_statusPanel.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_statusPanel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		statusPanel.setLayout(gbl_statusPanel);
		
		JLabel humidityValue = new JLabel("");
		GridBagConstraints gbc_humidityValue = new GridBagConstraints();
		gbc_humidityValue.insets = new Insets(0, 0, 5, 5);
		gbc_humidityValue.gridx = 0;
		gbc_humidityValue.gridy = 0;
		statusPanel.add(humidityValue, gbc_humidityValue);
		
		JLabel temperatureValue = new JLabel("");
		GridBagConstraints gbc_temperatureValue = new GridBagConstraints();
		gbc_temperatureValue.insets = new Insets(0, 0, 5, 5);
		gbc_temperatureValue.gridx = 0;
		gbc_temperatureValue.gridy = 0;
		statusPanel.add(temperatureValue, gbc_temperatureValue);
		
		JLabel lblNode = new JLabel("Node");
		GridBagConstraints gbc_lblNode = new GridBagConstraints();
		gbc_lblNode.anchor = GridBagConstraints.WEST;
		gbc_lblNode.insets = new Insets(0, 0, 5, 5);
		gbc_lblNode.gridx = 1;
		gbc_lblNode.gridy = 1;
		statusPanel.add(lblNode, gbc_lblNode);
		
		JComboBox comboBox_1 = new JComboBox();
		GridBagConstraints gbc_comboBox_1 = new GridBagConstraints();
		gbc_comboBox_1.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBox_1.insets = new Insets(0, 0, 5, 0);
		gbc_comboBox_1.gridwidth = 2;
		gbc_comboBox_1.gridx = 2;
		gbc_comboBox_1.gridy = 1;
		statusPanel.add(comboBox_1, gbc_comboBox_1);
		
		//Status panel element (humidity)
		JLabel lblNewLabel = new JLabel("Humidity");
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel.gridx = 1;
		gbc_lblNewLabel.gridy = 2;
		statusPanel.add(lblNewLabel, gbc_lblNewLabel);
		
		JCheckBox checkBox = new JCheckBox("");
		GridBagConstraints gbc_checkBox = new GridBagConstraints();
		gbc_checkBox.insets = new Insets(0, 0, 5, 5);
		gbc_checkBox.gridx = 2;
		gbc_checkBox.gridy = 2;
		statusPanel.add(checkBox, gbc_checkBox);
		
		JButton btnNewButton_1 = new JButton("");
		btnNewButton_1.setBackground(Color.RED);
		GridBagConstraints gbc_btnNewButton_1 = new GridBagConstraints();
		gbc_btnNewButton_1.fill = GridBagConstraints.BOTH;
		gbc_btnNewButton_1.insets = new Insets(0, 0, 5, 0);
		gbc_btnNewButton_1.gridx = 3;
		gbc_btnNewButton_1.gridy = 2;
		statusPanel.add(btnNewButton_1, gbc_btnNewButton_1);
		
		//Status panel element (temperature)
		JLabel lblNewLabel_1 = new JLabel("Temperature");
		GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
		gbc_lblNewLabel_1.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_1.insets = new Insets(0, 0, 0, 5);
		gbc_lblNewLabel_1.gridx = 1;
		gbc_lblNewLabel_1.gridy = 3;
		statusPanel.add(lblNewLabel_1, gbc_lblNewLabel_1);
		
		JCheckBox chckbxNewCheckBox = new JCheckBox("");
		GridBagConstraints gbc_chckbxNewCheckBox = new GridBagConstraints();
		gbc_chckbxNewCheckBox.insets = new Insets(0, 0, 0, 5);
		gbc_chckbxNewCheckBox.gridx = 2;
		gbc_chckbxNewCheckBox.gridy = 3;
		statusPanel.add(chckbxNewCheckBox, gbc_chckbxNewCheckBox);
		
		JButton btnNewButton_2 = new JButton("");
		btnNewButton_2.setMaximumSize(new Dimension(20, 9));
		btnNewButton_2.setMinimumSize(new Dimension(20, 9));
		btnNewButton_2.setPreferredSize(new Dimension(20, 9));
		GridBagConstraints gbc_btnNewButton_2 = new GridBagConstraints();
		gbc_btnNewButton_2.fill = GridBagConstraints.BOTH;
		gbc_btnNewButton_2.gridx = 3;
		gbc_btnNewButton_2.gridy = 3;
		statusPanel.add(btnNewButton_2, gbc_btnNewButton_2);
		
		
		//event panel
		JPanel EventPanel = new JPanel();
		EventPanel.setBorder(new TitledBorder(null, "Event Log", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagConstraints gbc_EventPanel = new GridBagConstraints();
		gbc_EventPanel.insets = new Insets(0, 0, 0, 5);
		gbc_EventPanel.fill = GridBagConstraints.BOTH;
		gbc_EventPanel.gridx = 0;
		gbc_EventPanel.gridy = 1;
		frmSensorNetwork.getContentPane().add(EventPanel, gbc_EventPanel);
		EventPanel.setLayout(new BorderLayout(0, 0));
		
		JScrollPane scrollPane = new JScrollPane();
		EventPanel.add(scrollPane);
		
		TextArea textArea = new TextArea();
		scrollPane.setViewportView(textArea);
		
		
		//control panel
		JPanel ControlPanel = new JPanel();
		ControlPanel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Controls", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagConstraints gbc_ControlPanel = new GridBagConstraints();
		gbc_ControlPanel.fill = GridBagConstraints.BOTH;
		gbc_ControlPanel.gridx = 1;
		gbc_ControlPanel.gridy = 1;
		frmSensorNetwork.getContentPane().add(ControlPanel, gbc_ControlPanel);
		ControlPanel.setLayout(new BorderLayout(0, 0));
		
		JScrollPane scrollPane_1 = new JScrollPane();
		ControlPanel.add(scrollPane_1, BorderLayout.CENTER);
		
		JPanel controlSubPanel = new JPanel();
		controlSubPanel.setForeground(Color.WHITE);
		controlSubPanel.setBorder(new EmptyBorder(2, 2, 2, 2));
		scrollPane_1.setViewportView(controlSubPanel);
		GridBagLayout gbl_controlSubPanel = new GridBagLayout();
		gbl_controlSubPanel.columnWidths = new int[] {0, 0, 0, 0};
		gbl_controlSubPanel.rowHeights = new int[]{0, 0, 0, 0, 0};
		gbl_controlSubPanel.columnWeights = new double[]{1.0, 1.0, 0.0, Double.MIN_VALUE};
		gbl_controlSubPanel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		controlSubPanel.setLayout(gbl_controlSubPanel);
		
		JLabel lblNewLabel_2 = new JLabel("Node");
		GridBagConstraints gbc_lblNewLabel_2 = new GridBagConstraints();
		gbc_lblNewLabel_2.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_2.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_2.gridx = 0;
		gbc_lblNewLabel_2.gridy = 0;
		controlSubPanel.add(lblNewLabel_2, gbc_lblNewLabel_2);
		
		JComboBox comboBox = new JComboBox();
		GridBagConstraints gbc_comboBox = new GridBagConstraints();
		gbc_comboBox.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBox.gridwidth = 2;
		gbc_comboBox.insets = new Insets(0, 0, 5, 0);
		gbc_comboBox.gridx = 1;
		gbc_comboBox.gridy = 0;
		controlSubPanel.add(comboBox, gbc_comboBox);
		
		//control panel element
		JLabel lblReportInterval = new JLabel("Interval");
		GridBagConstraints gbc_lblReportInterval = new GridBagConstraints();
		gbc_lblReportInterval.insets = new Insets(0, 0, 5, 5);
		gbc_lblReportInterval.anchor = GridBagConstraints.WEST;
		gbc_lblReportInterval.gridx = 0;
		gbc_lblReportInterval.gridy = 1;
		controlSubPanel.add(lblReportInterval, gbc_lblReportInterval);
		
		textField = new JTextField();
		GridBagConstraints gbc_textField = new GridBagConstraints();
		gbc_textField.insets = new Insets(0, 0, 5, 5);
		gbc_textField.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField.gridx = 1;
		gbc_textField.gridy = 1;
		controlSubPanel.add(textField, gbc_textField);
		textField.setColumns(10);
		
		JButton btnNewButton = new JButton("Send");
		GridBagConstraints gbc_btnNewButton = new GridBagConstraints();
		gbc_btnNewButton.insets = new Insets(0, 0, 5, 0);
		gbc_btnNewButton.gridx = 2;
		gbc_btnNewButton.gridy = 1;
		controlSubPanel.add(btnNewButton, gbc_btnNewButton);
		
		JLabel lblNewLabel_3 = new JLabel("New label");
		GridBagConstraints gbc_lblNewLabel_3 = new GridBagConstraints();
		gbc_lblNewLabel_3.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_3.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_3.gridx = 0;
		gbc_lblNewLabel_3.gridy = 2;
		controlSubPanel.add(lblNewLabel_3, gbc_lblNewLabel_3);
		
		JSpinner spinner = new JSpinner();
		spinner.setModel(new SpinnerNumberModel(0, 0, 1, 1));
		GridBagConstraints gbc_spinner = new GridBagConstraints();
		gbc_spinner.insets = new Insets(0, 0, 5, 5);
		gbc_spinner.gridx = 1;
		gbc_spinner.gridy = 2;
		controlSubPanel.add(spinner, gbc_spinner);
		
		JButton btnSend = new JButton("Send");
		GridBagConstraints gbc_btnSend = new GridBagConstraints();
		gbc_btnSend.insets = new Insets(0, 0, 5, 0);
		gbc_btnSend.gridx = 2;
		gbc_btnSend.gridy = 2;
		controlSubPanel.add(btnSend, gbc_btnSend);
		
		//JMenuBar menuBar = new JMenuBar();
		//frmSensorNetwork.setJMenuBar(menuBar);
		
		//JMenu mnProperties = new JMenu("Properties");
		//menuBar.add(mnProperties);
		
		//JMenuItem mntmSettings = new JMenuItem("Settings");
		//mnProperties.add(mntmSettings);
		
	}

}
