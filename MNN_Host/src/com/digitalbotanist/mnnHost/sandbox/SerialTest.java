package com.digitalbotanist.mnnHost.sandbox;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;

import gnu.io.CommPortIdentifier; 
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent; 
import gnu.io.SerialPortEventListener; 

import java.util.Calendar;
import java.util.Enumeration;

import com.digitalbotanist.javaPlots.FinePoint;
import com.digitalbotanist.javaPlots.TimePlot;


public class SerialTest implements SerialPortEventListener {
	TimePlot window;
	SerialTest(TimePlot window){
		this.window = window;
		initialize();
		Thread t=new Thread() {
			public void run() {
				//the following line will keep this app alive for 1000 seconds,
				//waiting for events to occur and responding to them (printing incoming messages to console).
				try {Thread.sleep(1000000);} catch (InterruptedException ie) {}
			}
		};
		t.start();
		System.out.println("Started");
	}
	
	SerialPort serialPort;
        /** The port we're normally going to use. */
	private static final String PORT_NAMES[] = { 
			"/dev/tty.usbserial-A9007UX1", // Mac OS X
			"/dev/ttyUSB0", // Linux
			"COM3", // Windows
			"COM5"
	};
	/**
	* A BufferedReader which will be fed by a InputStreamReader 
	* converting the bytes into characters 
	* making the displayed results codepage independent
	*/
	private BufferedReader input;
	/** The output stream to the port */
	private OutputStream output;
	/** Milliseconds to block while waiting for port open */
	private static final int TIME_OUT = 2000;
	/** Default bits per second for COM port. */
	private static final int DATA_RATE = 9600;
	
//	public void initialize2() {
//		CommPortIdentifier portId = null;
//		Enumeration portEnum = CommPortIdentifier.getPortIdentifiers();
//		int numElements = 0;
//		System.out.println("port i2c is: " + CommPortIdentifier.PORT_I2C);
//		System.out.println("port parallel is: " + CommPortIdentifier.PORT_PARALLEL);
//		System.out.println("port raw is: " + CommPortIdentifier.PORT_RAW);
//		System.out.println("port rs485 is: " + CommPortIdentifier.PORT_RS485);
//		System.out.println("port serial is: " + CommPortIdentifier.PORT_SERIAL);
//		//First, Find an instance of serial port as set in PORT_NAMES.
//		while (portEnum.hasMoreElements()) {
//			CommPortIdentifier currPortId = (CommPortIdentifier) portEnum.nextElement();
//			System.out.println(currPortId.toString());		
//			System.out.println(currPortId.getName() + " is a: " + currPortId.getPortType() );
//			numElements++;
//		}
//		System.out.println("done printing elements: " +numElements);
//	}
	
	public void initialize() {
		CommPortIdentifier portId = null;
		Enumeration portEnum = CommPortIdentifier.getPortIdentifiers();

		//First, Find an instance of serial port as set in PORT_NAMES.
		while (portEnum.hasMoreElements()) {
			CommPortIdentifier currPortId = (CommPortIdentifier) portEnum.nextElement();
			System.out.println("Found comm port: " + currPortId.getName());
			for (String portName : PORT_NAMES) {
				if (currPortId.getName().equals(portName)) {
					portId = currPortId;
					break;
				}
			}
		}
		if (portId == null) {
			System.out.println("Could not find COM port.");
			return;
		}

		try {
			// open serial port, and use class name for the appName.
			serialPort = (SerialPort) portId.open(this.getClass().getName(),
					TIME_OUT);

			// set port parameters
			serialPort.setSerialPortParams(DATA_RATE,
					SerialPort.DATABITS_8,
					SerialPort.STOPBITS_1,
					SerialPort.PARITY_NONE);

			// open the streams
			input = new BufferedReader(new InputStreamReader(serialPort.getInputStream()));
			output = serialPort.getOutputStream();

			// add event listeners
			serialPort.addEventListener(this);
			serialPort.notifyOnDataAvailable(true);
			System.out.println("Connected to port: " + serialPort.getName());
		} catch (Exception e) {
			System.err.println(e.toString());
		}
	}

	/**
	 * This should be called when you stop using the port.
	 * This will prevent port locking on platforms like Linux.
	 */
	public synchronized void close() {
		if (serialPort != null) {
			serialPort.removeEventListener();
			serialPort.close();
		}
	}

	/**
	 * Handle an event on the serial port. Read the data and print it.
	 */
	public synchronized void serialEvent(SerialPortEvent oEvent) {
		if (oEvent.getEventType() == SerialPortEvent.DATA_AVAILABLE) {
			String inputLine;
			try {
				 Calendar.getInstance().getTime();
				inputLine = input.readLine();
				System.out.println(inputLine);

				String[] tokens = inputLine.split(":");
				String inchesTemp = tokens[3];
				String inches = inchesTemp.split(" ")[0];
				inches.trim();
				
				double value = Double.parseDouble(inches);
				if(value < 213){
					//System.out.println("adding point to graph: " + value);
					window.addPoint(new FinePoint(value,value));
				}
			} catch (IOException e) {
				//e.printStackTrace();
			}
		}
	}

	public static void main(String[] args) throws Exception {
		//SerialTest main = new SerialTest();
		//main.initialize();
		Thread t=new Thread() {
			public void run() {
				//the following line will keep this app alive for 1000 seconds,
				//waiting for events to occur and responding to them (printing incoming messages to console).
				try {Thread.sleep(1000000);} catch (InterruptedException ie) {}
			}
		};
		t.start();
		System.out.println("Started");
	}
}