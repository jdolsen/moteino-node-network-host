package com.digitalbotanist.mnnHost.sandbox;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;

import com.digitalbotanist.javaPlots.BasePlot;
import com.digitalbotanist.javaPlots.MultiPlot;
import com.digitalbotanist.javaPlots.ScatterPlot;

public class FFTTest {
	public ArrayList<Complex> data = new ArrayList<Complex>();
	private final int arraySize = 128;
	private final int log2ArraySize = (int) (Math.log10(arraySize)/Math.log10(2));
	private double tau = Math.PI * 2;
	private int mult = 10;
	double w = 1;
	
	Complex [][] wArrayCplx = new Complex [log2ArraySize][arraySize/2];
	double [][] wArray = new double[log2ArraySize][arraySize/2];
	double [][] wSin = new double[log2ArraySize][arraySize/2];
	double [][] wCos = new double[log2ArraySize][arraySize/2];
	
	void initData(){
		for(int i = 0; i < arraySize; ++i){
			int f1 = (int) (mult * Math.cos(tau*i/arraySize));
			double f2 =  (mult * Math.cos(2*tau*i/arraySize));
			int f3 = (int) (mult * Math.cos(3*tau*i/arraySize));
			int f4 = (int) (mult * Math.cos(4*tau*i/arraySize));
			int f5 = (int) (mult * Math.cos(5*tau*i/arraySize));
			int f6 = (int) (mult * Math.cos(6*tau*i/arraySize));
			int f7 = (int) (mult * Math.cos(7*tau*i/arraySize));
			int f8 = (int) (mult * Math.cos(8*tau*i/arraySize));
			int f15 = (int) (mult * Math.cos(15*tau*i/arraySize));
			int f31 = (int) (mult * Math.sin(31*tau*i/arraySize));
			int f63 = (int) (mult * Math.cos(63*tau*i/arraySize));
			int f100 = (int) (mult *10* Math.cos(128*tau*i/arraySize));
			double noise =0 * mult * Math.random();
			data.add(new Complex(f1+f2+f3+f5+f7+f15+f31+f63+noise));//add some summation of the various frequency values to the data array
		}
		
		//window(data);
		
		//generate list of w values
		for(int l = 0; l < log2ArraySize; ++l){
			//int numJ = 2^(l);
	
			int numi = (int) Math.pow(2,l);
			int div = (int) Math.pow(2,l+1);
			//System.out.println(numJ);
			for(int i = 0; i< numi; i++){
				//double w = -tau * i/(numJ);
				
				double re = Math.cos(tau*i/div);
				double im = -Math.sin(tau*i/div);
				double tot = re-im;
				wArrayCplx[l][i] = new Complex(re, im);
				wArray[l][i] = tot;// wArrayCplx[l][i].getNorm();
				wSin[l][i] = -im;
				wCos[l][i] = re;
			}
		}
		
		System.out.println("List of W values:");
		for(int l = 0; l < log2ArraySize; ++l){
			int numJ = (int) Math.pow(2,l);
			
			System.out.print("[");
			for(int j = 0; j< numJ; j++){
				System.out.print(wArrayCplx[l][j] + ", ");
				//wArray[l][j] = Math.exp(w);
			}
			System.out.println("]");
		}
		System.out.println("");
		
	}
	
	private void window(ArrayList<Complex> data2) {
		int endSize = arraySize/23;
		int tailIndex = arraySize-endSize-1;
		for(int i = 0; i< endSize; ++i){
			Complex multiplier = new Complex(((double)i)/endSize, 0);			
			data2.set(i, data2.get(i).mult(multiplier));
			
			multiplier = new Complex(((double)i)/endSize, 0);
			data2.set(arraySize-i-1, data2.get(arraySize-i-1).mult(multiplier));			
		}
		
		
//		for(int i = arraySize-1; i> tailIndex; --i){
//			Complex multiplier = new Complex(((double)i)/endSize, 0);			
//			data2.set(i+tailIndex, data2.get(i+tailIndex).mult(multiplier));
//		}		
	}

	void initTestData(){
		for(int i = 0; i < 64; ++i){
			data.add(new Complex(i));
		}
	}
	
	public ArrayList<Complex> doFFT(){
		//decimate data
		ArrayList<Complex> newData = decimate(data);
		ArrayList<Complex> newData2 = arrayFFT(newData, 0);
		return newData2;
		
	}

	//recursively reorders the array (odds/evens)
	private ArrayList<Complex> decimate(List<Complex> list) {
		int halfPoint = list.size()/2;//size will always be a multiple of 2
		//System.out.println(halfPoint);
		ArrayList<Complex> mix = new ArrayList<Complex>();
		ArrayList<Complex> result = new ArrayList<Complex>();
		mix.addAll(getOddEvenElements(list, false));
		mix.addAll(getOddEvenElements(list, true));
		if(halfPoint >2){
			result.addAll(decimate(mix.subList(0, halfPoint)));
			result.addAll(decimate(mix.subList(halfPoint, mix.size())));
			return result;
		}else{
			return mix;
		}		
	}
	
	private ArrayList<Complex> getOddEvenElements(List<Complex> list, boolean isOdd){
		ArrayList<Complex> result = new ArrayList<Complex>();
		int mod = 0;
		if(isOdd){
			mod = 1;
		}
		for(int i = 0; i< list.size(); ++i){
			if(i%2 == mod){
				result.add(list.get(i));
			}
		}		
		return result;
	}
	
	//takes a decimated array with a size that is a power of 2 
	//it divides the array in half and returns the resulting combined FFT array
	//this means the smallest array size is 2
	private ArrayList<Complex> arrayFFT(List<Complex> list, int level){
		int halfPoint = list.size()/2;//size will always be a multiple of 2
		//if the halfPoint size is greater than 1, do two recursive calls on the 2 halves of the array
		List<Complex> mix = new ArrayList<Complex>();
		ArrayList<Complex> result = new ArrayList<Complex>();
		//System.out.println(halfPoint);
		if(halfPoint>1){
			mix.addAll(arrayFFT(list.subList(0, halfPoint), level + 1));
			mix.addAll(arrayFFT(list.subList(halfPoint, list.size()), level+ 1));
			
		}else{
			mix = list;
		}
		
		//(if we used a pre-sized array, we could do these
		//next two for loops together)
		
		//add the positive results to the array
		for(int i = 0; i<halfPoint; ++i){
			result.add((mix.get(i).add((wArrayCplx[log2ArraySize-1-level][i].mult( mix.get(halfPoint+i))))));			
		}
		//add the negative results to the array  
		for(int i = 0; i<halfPoint; ++i){
			result.add((mix.get(i).sub((wArrayCplx[log2ArraySize-1-level][i].mult(mix.get(halfPoint+i))))));
		}
		
		return result;
	}
	
	private ArrayList<Complex> arrayFHT(List<Complex> list, int level){
		int halfPoint = list.size()/2;//size will always be a multiple of 2
		//if the halfPoint size is greater than 1, do two recursive calls on the 2 halves of the array
		List<Complex> mix = new ArrayList<Complex>();
		ArrayList<Complex> result = new ArrayList<Complex>();
		//System.out.println(halfPoint);
		if(halfPoint>1){
			mix.addAll(arrayFFT(list.subList(0, halfPoint), level + 1));
			mix.addAll(arrayFFT(list.subList(halfPoint, list.size()), level+ 1));
			
		}else{
			mix = list;
		}
				
		//add the positive results to the array
		for(int i = 0; i<halfPoint; ++i){
			//double w = wArray[log2ArraySize-1-level][i];
			double sin = wSin[log2ArraySize-1-level][i];
			double cos = wCos[log2ArraySize-1-level][i];
			result.add(new Complex(cos*mix.get(i).re + (sin * mix.get(halfPoint+i).re)));			
		}
		
		//add the negative results to the array  
		for(int i = 0; i<halfPoint; ++i){
			//double w = wArray[log2ArraySize-1-level][i];
			double sin = wSin[log2ArraySize-1-level][i];
			double cos = wCos[log2ArraySize-1-level][i];
			result.add(new Complex(sin*mix.get(i).re - (cos * mix.get(halfPoint+i).re)));
		}
		
		return result;
	}
	
	
	public static void printArray(List<Complex> list){
		System.out.print("[");
		for(Complex i: list){
//			if(i<0){
//				System.out.print("0, ");
//			}else{
				System.out.print(i + ",");
//			}
		}
		System.out.println("]");
	}
	
	public static void graphArrayNorm(List<Complex> list, BasePlot p){
        for(int i=0; i <list.size();++i){
        	p.addPoint(i, list.get(i).getNorm());
        	//System.out.print(fftList.get(i) + ", ");
        }
	}
	
	public static void graphArrayRe(List<Complex> list, BasePlot p){
        for(int i=0; i <list.size();++i){
        	p.addPoint(i, list.get(i).getRe());
        	//System.out.print(fftList.get(i) + ", ");
        }
	}
	
	public static void graphArrayIm(List<Complex> list, BasePlot p){
        for(int i=0; i <list.size();++i){
        	p.addPoint(i, list.get(i).getIm());
        	//System.out.print(fftList.get(i) + ", ");
        }
	}
		
	public static void main(String args[]){
		FFTTest myTest = new FFTTest();
		myTest.initData();
		printArray(myTest.data);
		printArray(myTest.decimate(myTest.data));
		ArrayList<Complex> newData = myTest.decimate(myTest.data);
		ArrayList<Complex> fftList =myTest.arrayFFT(newData, 0); 
		ArrayList<Complex> fhtList =myTest.arrayFHT(newData, 0); 
		printArray(fftList);
		
	       JFrame f = new JFrame();
	       f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//	       final ScatterPlot graph = new ScatterPlot();
//	       f.add(graph);
	       
	       final MultiPlot plots = new MultiPlot();
	       final ScatterPlot graph1 = new ScatterPlot(Color.red);
	       final ScatterPlot graph2 = new ScatterPlot(Color.green);
	       final ScatterPlot graph3 = new ScatterPlot(Color.blue);
	       //plots.addPlot(graph1);
	       plots.addPlot(graph2, "graph2");
	       plots.addPlot(graph3, "graph3");
	       graph2.setXAutoScale(true);
	       graph2.setYAutoScale(true);
	       f.add(plots);
	       //plots.paintChildren(f.getGraphics());
	       f.setSize(600,400);
	       f.setLocation(600,200);
	       f.setVisible(true);
	        	 	       
	        myTest.graphArrayNorm(myTest.data, graph2);
	       //myTest.graphArrayNorm(fftList.subList(0, fftList.size()/2), graph1);
	       //myTest.graphArrayRe(fhtList.subList(0, fhtList.size()/2), graph2);
	       //myTest.graphArrayRe(fftList.subList(0, fftList.size()/2), graph2);
	       myTest.graphArrayNorm(fftList.subList(0, fftList.size()/2), graph3);
	       
	       //myTest.graphArrayRe(myTest.data, graph2);
	       //myTest.graphArrayIm(myTest.data, graph3);
//	        for(int i=0; i <fftList.size();++i){
//	        	graph.addPoint(i, fftList.get(i));
//	        	//System.out.print(fftList.get(i) + ", ");
//	        }
	        //graph.drawPoints(g2);
	}
}
