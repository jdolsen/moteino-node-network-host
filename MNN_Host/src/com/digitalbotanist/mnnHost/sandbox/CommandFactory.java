package com.digitalbotanist.mnnHost.sandbox;
import java.util.ArrayList;
import java.util.List;


public class CommandFactory {

	private static CommandFactory instance = null;
	
	private CommandFactory(){}
	
	public static CommandFactory getInstance(){
		if(instance == null){
			instance = new CommandFactory();
		}
		
		return instance;
	}
	
	//public Command getSetIntervalCommand(){
	//	List<String> fields = new ArrayList<String>();
		//fields.add("interval");
		//Command c = new Command("SetRef", fields);
		
	//	return c;
	//}
}
