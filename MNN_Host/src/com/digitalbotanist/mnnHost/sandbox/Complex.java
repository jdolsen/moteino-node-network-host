package com.digitalbotanist.mnnHost.sandbox;

public class Complex {
	public double re;
	public double im;

	public Complex() {
		re = 0;
		im = 0;
	}

	public Complex(double re, double im) {
		this.re = re;
		this.im = im;
	}
	
	public Complex(double re) {
		this.re = re;
		this.im = 0;
	}

	public Complex(Complex in) {
		this.re = in.re;
		this.im = in.im;
	}

	public double getRe() {
		return re;
	}

	public void setRe(double re) {
		this.re = re;
	}

	public double getIm() {
		return im;
	}

	public void setIm(double im) {
		this.im = im;
	}

	public Complex getConjugate() {
		return new Complex(this.re, this.im * (-1));
	}

	public Complex add(Complex op) {
		Complex result = new Complex();
		result.setRe(this.re + op.getRe());
		result.setIm(this.im + op.getIm());
		return result;
	}

	public Complex sub(Complex op) {
		Complex result = new Complex();
		result.setRe(this.re - op.getRe());
		result.setIm(this.im - op.getIm());
		return result;
	}

	public Complex mult(Complex op) {
		Complex result = new Complex();
		result.setRe(this.re * op.getRe() - this.im * op.getIm());
		result.setIm(this.re * op.getIm() + this.im * op.getRe());
		return result;
	}

	public Complex div(Complex op) {
		Complex result = new Complex(this);
		result = result.mult(op.getConjugate());
		double opNormSq = op.getRe() * op.getRe() + op.getIm() * op.getIm();
		result.setRe(result.getRe() / opNormSq);
		result.setIm(result.getIm() / opNormSq);
		return result;
	}

	public Complex fromPolar(double magnitude, double angle) {
		Complex result = new Complex();
		result.setRe(magnitude * Math.cos(angle));
		result.setIm(magnitude * Math.sin(angle));
		return result;
	}

	public double getNorm() {
		return Math.sqrt(this.re * this.re + this.im * this.im);
	}

	public double getAngle() {
		return Math.atan2(this.im, this.re);
	}

	public String toString() {
		if (this.re == 0) {
			if (this.im == 0) {
				return "0";
			} else {
				return (this.im + "i");
			}
		} else {
			if (this.im == 0) {
				return String.valueOf(this.re);
			} else if (this.im < 0) {
				return (this.re + " " + this.im + "i");
			} else {
				return (this.re + " +" + this.im + "i");
			}
		}
	}
}

