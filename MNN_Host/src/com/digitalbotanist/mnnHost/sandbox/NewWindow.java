package com.digitalbotanist.mnnHost.sandbox;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.BoxLayout;
import javax.swing.JTextField;
import java.awt.Component;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JPanel;


public class NewWindow {

	private JFrame frame;
	private JTextField txtBlahBlahBlah;
	private JTextField textField;
	private JLabel lblNewLabel;
	private JPanel panel;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					NewWindow window = new NewWindow();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public NewWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.Y_AXIS));
		
		txtBlahBlahBlah = new JTextField();
		txtBlahBlahBlah.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtBlahBlahBlah.setToolTipText("bbbbbb");
		txtBlahBlahBlah.setText("blah blah blah");
		txtBlahBlahBlah.setAlignmentY(Component.TOP_ALIGNMENT);
		txtBlahBlahBlah.setAlignmentX(Component.LEFT_ALIGNMENT);
		frame.getContentPane().add(txtBlahBlahBlah);
		txtBlahBlahBlah.setColumns(8);
		
		textField = new JTextField();
		textField.setAlignmentY(Component.TOP_ALIGNMENT);
		textField.setAlignmentX(Component.LEFT_ALIGNMENT);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		panel = new JPanel();
		frame.getContentPane().add(panel);
		panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
		
		lblNewLabel = new JLabel("New label");
		panel.add(lblNewLabel);
		
	}

}
