package com.digitalbotanist.mnnHost.sandbox;

public class Field {
	Class type;
	Object value;
	
	public Field(Class type, Object value){
		this.type = type;
		this.value = value;
	}
	
	public Class getType() {
		return type;
	}

	public void setType(Class type) {
		this.type = type;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}
}
