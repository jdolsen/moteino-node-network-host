package com.digitalbotanist.mnnHost;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import com.digitalbotanist.mnnHost.events.MailEvent;
import com.digitalbotanist.mnnHost.valueHandler.AmountChangedValueHandler;
import com.digitalbotanist.mnnHost.valueHandler.AnyValueHandler;
import com.digitalbotanist.mnnHost.valueHandler.ChangedValueHandler;
import com.digitalbotanist.mnnHost.valueHandler.EqualValueHandler;
import com.digitalbotanist.mnnHost.valueHandler.InRangeValueHandler;
import com.digitalbotanist.mnnHost.valueHandler.MaxValueHandler;
import com.digitalbotanist.mnnHost.valueHandler.MinValueHandler;
import com.digitalbotanist.mnnHost.valueHandler.OutRangeValueHandler;
import com.digitalbotanist.mnnHost.valueHandler.PercentChangedValueHandler;
import com.digitalbotanist.mnnHost.valueHandler.ValueHandler;

//TODO: change this so that event objects are more generic and have an action listener interface
//TODO: abstract action things (like sending a text, email, response message, or http call) into the action interface
public class DataEventHandler extends JPanel{

	private static final long serialVersionUID = 1L;
	HashMap<String, Double> dataItems = new HashMap<String, Double>();
	ArrayList<ValueHandler> handlers = new ArrayList<ValueHandler>();
	
	JPanel eventCreation = new JPanel();
	JPanel eventPanel = new JPanel();
	JPanel eventList = new JPanel();
	
	JTextField adxField = null;
	JPasswordField pwField = null;
	JTextField dadxField = null;

	JTextField smtpAuthField = null;
	JTextField smtpTlsField = null;
	JTextField smtpHostField = null;
	JTextField smtpPortField = null;
	
	JTextField pop3TlsField = null;
	JTextField pop3HostField = null;
	JTextField pop3PortField = null;

	
	public DataEventHandler(){
		this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		populateEventListPanel();
		populateCreationPanel();
		this.add(eventCreation);
		this.add(new JSeparator(JSeparator.VERTICAL));
		
		
		eventPanel.setLayout(new BoxLayout(eventPanel, BoxLayout.PAGE_AXIS));
		eventPanel.add(new JLabel("Events"));
		//eventPanel.add(new JSeparator(JSeparator.HORIZONTAL));		
		eventPanel.add(eventList);
		this.add(eventPanel);
	}
	
	private void populateEventListPanel(){
		eventList.setLayout(new BoxLayout(eventList, BoxLayout.Y_AXIS));
		
		JButton b = new JButton("Remove Selected");
		
		b.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent arg0) {
					Iterator<ValueHandler> i = handlers.iterator();
					while(i.hasNext()){
						ValueHandler h = i.next();
						if(h.getCheck().isSelected()){
							i.remove();
							eventList.remove(h);
						}					
					}//end while	
					eventList.revalidate();
					eventList.repaint();
				}
			});
		
		eventList.add(b);
	}
	
	private void populateCreationPanel(){
		eventCreation.setLayout(new BorderLayout());				
		Box eventCreationBox = Box.createVerticalBox();			
		adxField = addLabeledTextField("E-mail Address", null, eventCreationBox);
		//pwField = new JPasswordField();
		
		Box pwCreateBox = Box.createHorizontalBox();	
		JLabel l = new JLabel("Password");
		l.setAlignmentX(LEFT_ALIGNMENT);
		pwCreateBox.add(l);
		pwCreateBox.add(Box.createHorizontalStrut(10));
		pwField = new JPasswordField();
		pwField.setFont(new Font("Tahoma", Font.PLAIN, 16));
		pwField.setPreferredSize(new Dimension(200,20));
		//text.setAlignmentX(LEFT_ALIGNMENT);
		//pwField.setText(null);
		pwCreateBox.add(pwField);
		pwCreateBox.add(Box.createHorizontalGlue());
		pwCreateBox.add(Box.createVerticalGlue());
		eventCreationBox.add(pwCreateBox);
		
		dadxField = addLabeledTextField("Destination Address", null, eventCreationBox);

		smtpAuthField = addLabeledTextField("SMTP Authorization", "true", eventCreationBox);
		smtpTlsField = addLabeledTextField("SMTP tls enabled", "true", eventCreationBox);
		smtpHostField = addLabeledTextField("SMTP host", "smtp.gmail.com", eventCreationBox);
		smtpPortField = addLabeledTextField("SMTP port", "587", eventCreationBox);
		
		pop3TlsField = addLabeledTextField("POP3 tls enabled", "true", eventCreationBox);
		pop3HostField = addLabeledTextField("POP3 host", "pop.gmail.com", eventCreationBox);
		pop3PortField = addLabeledTextField("POP3 port", "995", eventCreationBox);
				
		Box createBox = Box.createHorizontalBox();	
		createBox.add(new JLabel("Trigger Type"));
		createBox.add(Box.createHorizontalStrut(10));
		JComboBox<String> triggerTypeBox = new JComboBox<String>();
		triggerTypeBox.addItem("Greater Than Max");
		triggerTypeBox.addItem("Less Than Min");
		triggerTypeBox.addItem("In Range");
		triggerTypeBox.addItem("Out of Range");
		triggerTypeBox.addItem("Any Value");
		triggerTypeBox.addItem("Value Changed");
		triggerTypeBox.addItem("Changed by Amount");
		triggerTypeBox.addItem("Changed by Percent");
		createBox.add(triggerTypeBox);
		eventCreationBox.add(createBox);
		
		/*
		 * TODO: place holder for additional event types
		createBox = Box.createHorizontalBox();	
		createBox.add(new JLabel("Event Type"));
		createBox.add(Box.createHorizontalStrut(10));
		JComboBox<String> eventTypeBox = new JComboBox<String>();
		eventTypeBox.addItem("Mail Event");
		eventTypeBox.addItem("Node Command Event");
		eventTypeBox.setEnabled(false);
		createBox.add(eventTypeBox);			
		eventCreationBox.add(createBox);
		*/
		JTextField v1Field = addLabeledTextField("Value 1", null, eventCreationBox); 
		JTextField v2Field = addLabeledTextField("Value 2", null, eventCreationBox); 		
		JTextField nnField = addLabeledTextField("Node Name", "123456789abc", eventCreationBox); 
		JTextField cField = addLabeledTextField("Command", "data", eventCreationBox); 
		JTextField fField = addLabeledTextField("Field", "F3", eventCreationBox); 
		
		JButton create = new JButton("Create");
		create.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				
				String idString = getDataItemString(nnField.getText(), cField.getText(), fField.getText());
				MailEvent event = new MailEvent(dadxField.getText(), adxField.getText(), pwField.getText());
				event.addProperty("mail.smtp.auth", smtpAuthField.getText());					
				
				event.addProperty("mail.smtp.starttls.enable", smtpTlsField.getText());
				event.addProperty("mail.smtp.host", smtpHostField.getText());
				event.addProperty("mail.smtp.port", smtpPortField.getText());
				
				event.addProperty("mail.pop3s.starttls.enable", pop3TlsField.getText());
				event.addProperty("mail.pop3s.host", pop3HostField.getText());
				event.addProperty("mail.pop3s.port", pop3PortField.getText());
				
				String triggerType = (String) triggerTypeBox.getSelectedItem();
				Double val1 = null;
				try{
					val1 = Double.parseDouble(v1Field.getText());
				}catch(Exception e){
					e.printStackTrace();
				}
				double val2 = 0d;
				
				System.out.println("Creating " + triggerType + " trigger for " + idString);
				
				switch(triggerType){
				case "Greater Than Max":
					addEventHandler(new MaxValueHandler(idString, val1, event));
					break;
				case "Less Than Min":
					addEventHandler(new MinValueHandler(idString, val1, event));
					break;
				case "In Range":
					val2 = Double.parseDouble(v2Field.getText());
					addEventHandler(new InRangeValueHandler(idString, val1, val2, event));
					break;
				case"Out of Range":
					val2 = Double.parseDouble(v2Field.getText());
					addEventHandler(new OutRangeValueHandler(idString, val1, val2, event));
					break;
				case"Equals Value":
					addEventHandler(new EqualValueHandler(idString, val1, event));
					break;	
				case"Any Value":
					addEventHandler(new AnyValueHandler(idString, event));
					break;
				case"Value Changed":
					addEventHandler(new ChangedValueHandler(idString, event));
					break;
				case"Changed by Amount":
					addEventHandler(new AmountChangedValueHandler(idString, val1, event));
					break;
				case"Changed by Percent":					
					addEventHandler(new PercentChangedValueHandler(idString, val1, event));
					break;
				default:
					break;				
				}
				
			}			
		});
		createBox = Box.createHorizontalBox();
		createBox.add(Box.createHorizontalGlue());
		createBox.add(create);

		
		eventCreationBox.add(Box.createVerticalStrut(10));
		eventCreationBox.add(createBox);
		eventCreationBox.add(Box.createVerticalStrut(100));
		eventCreationBox.setAlignmentX(LEFT_ALIGNMENT);
		eventCreation.setAlignmentX(LEFT_ALIGNMENT);
		eventCreation.add(eventCreationBox);
	}

	public void updateEventDataItem(String from, String command,
			String field, Double val) {
		dataItems.put(getDataItemString(from, command, field), val);		
	}
	
	public void addEventHandler(ValueHandler handler){
		String className = handler.getClass().getTypeName();
		String eventName = handler.getEvent().getClass().getTypeName();
		System.out.println("Adding handler of type:" + className + " with event of type " + eventName);
		handlers.add(handler);
		eventList.add(handler);
		this.revalidate();
		this.repaint();
	}


	public void updateHandlers() {
		for(ValueHandler v: handlers){
			v.trigger(dataItems);
		}		
	}
	
	public JTextField addLabeledTextField(String label, String defaultValue, Box panel){
		Box createBox = Box.createHorizontalBox();	
		JLabel l = new JLabel(label);
		l.setAlignmentX(LEFT_ALIGNMENT);
		createBox.add(l);
		createBox.add(Box.createHorizontalStrut(10));
		JTextField text = new JTextField();
		text.setFont(new Font("Tahoma", Font.PLAIN, 16));
		text.setPreferredSize(new Dimension(200,20));
		//text.setAlignmentX(LEFT_ALIGNMENT);
		text.setText(defaultValue);
		createBox.add(text);
		createBox.add(Box.createHorizontalGlue());
		createBox.add(Box.createVerticalGlue());
		panel.add(createBox);
		return text;
	}
	
	public String getDataItemString(String from, String commandString,
			String fieldname){
		return from+"|"+commandString+"|"+fieldname;
	}

	public void saveEvents(ObjectOutputStream objOut) {
		try {
			System.out.println("Saving events");
			//save the value fields
			objOut.writeUTF(adxField.getText());
			objOut.writeUTF(pwField.getText());
			objOut.writeUTF(dadxField.getText());

			objOut.writeUTF(smtpAuthField.getText());
			objOut.writeUTF(smtpTlsField.getText());
			objOut.writeUTF(smtpHostField.getText());
			objOut.writeUTF(smtpPortField.getText());
			
			objOut.writeUTF(pop3TlsField.getText());
			objOut.writeUTF(pop3HostField.getText());
			objOut.writeUTF(pop3PortField.getText());
			
			//save the handlers
			objOut.writeInt(handlers.size());
			for(ValueHandler h : handlers){
				objOut.writeObject(h);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}	
	}

	public void loadEvents(ObjectInputStream objIn) {
		try {
			//load the value fields	
			System.out.println("Loading events");
			handlers.clear();
			eventList.removeAll();
			populateEventListPanel();
			//byte[] read = (String) objIn.readObject();
			//objIn.readUTF()
			adxField.setText(objIn.readUTF());//new String(read));
			pwField.setText(objIn.readUTF());
			dadxField.setText(objIn.readUTF());

			smtpAuthField.setText(objIn.readUTF());
			smtpTlsField.setText(objIn.readUTF());
			smtpHostField.setText(objIn.readUTF());
			smtpPortField.setText(objIn.readUTF());
			
			pop3TlsField.setText(objIn.readUTF());
			pop3HostField.setText(objIn.readUTF());
			pop3PortField.setText(objIn.readUTF());	
			
			//load the handlers
			int numHandlers = objIn.readInt();
			for(int i = 0; i < numHandlers; ++i){
				ValueHandler handler = (ValueHandler) objIn.readObject();
				addEventHandler(handler);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}	
		
	}

}
