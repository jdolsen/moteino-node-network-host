package com.digitalbotanist.mnnHost;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Calendar;
import java.util.HashMap;

import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;

import com.digitalbotanist.javaPlots.FinePoint;
import com.digitalbotanist.javaPlots.MultiPlot;
import com.digitalbotanist.javaPlots.TimePlot;
import com.digitalbotanist.mnnHost.NodeMessageHandler.CommandFieldListNodeMessageHandler;
import com.digitalbotanist.mnnHost.NodeMessageHandler.CommandListNodeMessageHandler;
import com.digitalbotanist.mnnHost.NodeMessageHandler.NameNodeMessageHandler;
import com.digitalbotanist.mnnHost.NodeMessageHandler.NodeConnectNodeMessageHandler;
import com.digitalbotanist.mnnHost.NodeMessageHandler.NodeDisconnectNodeMessageHandler;
import com.digitalbotanist.mnnHost.NodeMessageHandler.NodeMessageHandler;
import com.digitalbotanist.mnnHost.serial.DataItem;
import com.digitalbotanist.mnnHost.serial.IParsedObjectReceiver;
import com.digitalbotanist.mnnHost.serial.NodeMessage;
import com.digitalbotanist.mnnHost.serial.ObjectType;
import com.digitalbotanist.mnnHost.serial.ParsedObject;
import com.digitalbotanist.mnnHost.serial.ParsedObjectCreator;
import com.digitalbotanist.mnnHost.serial.SerialConnector;
import com.digitalbotanist.mnnHost.serial.TypedObject;

import dataStreams.DataProcessingHandler;
import dataStreams.ProcessedStream;

public class MNN_Host implements IParsedObjectReceiver{
	
	public final int GRAPHTIME = 3600*8;
	final MultiPlot graphs = new MultiPlot();
	//JPanel frmSensorNetwork = new JPanel();
	
	
	StatusPanelCreator sPanel = null;
	SerialConnector conn = null;
	DataEventHandler eventHandler = new DataEventHandler();	
	DataProcessingHandler dataProcessingHandler = null;
	//TODO: flesh these objects out!
	//TODO: this should probably get 
	
	ActionsHandler actionHandler = new ActionsHandler(eventHandler);
	 
	NodeControlTab controlTab;
	
	
	private HashMap<Integer, Node> nodes = new HashMap<Integer, Node>();	
	private HashMap<String, NodeMessageHandler> messageHandlers = new HashMap<String, NodeMessageHandler>();
	  
	public void addTimePlot(String name, TimePlot newGraph){
		if(graphs.hasPlot(name)){
			//do nothing
		}else{
			newGraph.setYAutoScale(false);
			newGraph.setYAutoScale(false);
			System.out.println("Adding graph: " + name);
			graphs.addTimePlot(newGraph, name);
			sPanel.createStatusPanelElement(name);//random color is set here
		}		
	}
	
	public void addProcessedStream(String name, ProcessedStream newGraph){
		addTimePlot(name, newGraph);
		newGraph.setCommandField(sPanel.getStatusDisplayObject(name));
	
	}
	
	public void addNewGraph(String name){
		addTimePlot(name, new TimePlot(GRAPHTIME, Color.cyan));//color does not matter...
	}
	
	public void initialize(){
		//create the application window
		JFrame appFrame = new JFrame();
		appFrame.setBounds(100, 100, 1135, 742);
		appFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		appFrame.getContentPane().setLayout(new GridLayout(0, 1, 0, 0));
		try {
			Image image = ImageIO.read(getClass().getResource("/resources/chipIcon.png"));
			appFrame.setIconImage(image);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		appFrame.setTitle("Moteino Node Network");
		JMenuBar menuBar = new JMenuBar();
		appFrame.setJMenuBar(menuBar);
		
		
		JMenu gateway = new JMenu("Gateway");
		menuBar.add(gateway);
		
		JMenu gwConnect = new JMenu("Connect");
		gateway.add(gwConnect);
		
		JMenuItem gwDisconnect = new JMenuItem("Disconnect");
		gateway.add(gwDisconnect);
		
		gwDisconnect.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				conn.disconnectCurrentPort();
			}
		});
		
		conn = SerialConnector.getInstance();
		conn.addComSelections(gwConnect);
	
		JMenu networkMenu = new JMenu("Network");
		menuBar.add(networkMenu);
		
		JMenuItem saveNetwork = new JMenuItem("Save");
		JMenuItem loadNetwork = new JMenuItem("Load");
		
		networkMenu.add(saveNetwork);
		networkMenu.add(loadNetwork);
		
		saveNetwork.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				// choose a file to save network to
				JFileChooser choose = new JFileChooser();
				int val = choose.showOpenDialog(appFrame);
				if(val == JFileChooser.APPROVE_OPTION){
					try {
						File file = choose.getSelectedFile();
						FileOutputStream fOut = new FileOutputStream(file);
						ObjectOutputStream objOut = new ObjectOutputStream(fOut);
	
						//TODO: maybe save the nodes					
						
						// save the events
						eventHandler.saveEvents(objOut);
						
						objOut.close();
						fOut.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		});
		
		loadNetwork.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				// choose a file to load from
				JFileChooser choose = new JFileChooser();
				int val = choose.showOpenDialog(appFrame);
				if(val == JFileChooser.APPROVE_OPTION){
					try {
						File file = choose.getSelectedFile();
						FileInputStream fIn = new FileInputStream(file);
						ObjectInputStream objIn = new ObjectInputStream(fIn);
						//TODO: maybe load the number of nodes and the nodes
						
						//load the number of events and the events
						eventHandler.loadEvents(objIn);
					}catch(IOException e){

						e.printStackTrace();
					} 
				}
			}
		});
		
		//initialize the tabs
		JTabbedPane tabbedPane_1 = new JTabbedPane(JTabbedPane.TOP);
		JPanel dataTab = initializeDataTab();
		controlTab = initializeControlTab();	
		
		tabbedPane_1.addTab("Nodes", null, controlTab, null);
		tabbedPane_1.addTab("Data", null, dataTab, null);
		tabbedPane_1.addTab("Data Processing", null, dataProcessingHandler, null);
		tabbedPane_1.addTab("Actions", null, actionHandler, null);
		tabbedPane_1.addTab("Events", null, eventHandler, null);		
		
		tabbedPane_1.setSelectedIndex(1);
		appFrame.getContentPane().add(tabbedPane_1);
		appFrame.setVisible(true);  
		
		//initialize graph display
		GridBagConstraints gbc_graphPanel = new GridBagConstraints();
		gbc_graphPanel.insets = new Insets(0, 0, 5, 5);
		gbc_graphPanel.fill = GridBagConstraints.BOTH;
		gbc_graphPanel.gridx = 0;
		gbc_graphPanel.gridy = 0;
		dataTab.add(graphs, gbc_graphPanel);
		
		//initialize messageHandlers
		registerMessageHandler(new NodeDisconnectNodeMessageHandler());
		registerMessageHandler(new NodeConnectNodeMessageHandler());
		registerMessageHandler(new NameNodeMessageHandler());
		registerMessageHandler(new CommandFieldListNodeMessageHandler(this));
		registerMessageHandler(new CommandListNodeMessageHandler());
		
		dataProcessingHandler.init();
		//dataProcessingHandler.test();
	}
	
	public JPanel initializeDataTab(){
		JPanel frmSensorNetwork = new JPanel();
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] {762, 270, 0};
		gridBagLayout.rowHeights = new int[]{320, 200, 0};
		gridBagLayout.columnWeights = new double[]{1.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{1.0, 0.0, Double.MIN_VALUE};
		frmSensorNetwork.setLayout(gridBagLayout);
		
		ControlPanelCreator cPanel = new ControlPanelCreator(frmSensorNetwork);
		sPanel = new StatusPanelCreator(frmSensorNetwork, graphs);
		dataProcessingHandler = new DataProcessingHandler(this, sPanel, graphs);
		EventPanelCreator ePanel = new EventPanelCreator(frmSensorNetwork);
		
		conn.initialize(this, new ParsedObjectCreator(ePanel), nodes);
	
		cPanel.createControlPanelElement("Req Stat", new JTextField(), new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {				
				conn.sendSerial(new NodeMessage("CNREQ|" +99, 127));
				ePanel.addEvent("Sent status request to gateway");
			}
		});
				
		JTextField newID = new JTextField();
		cPanel.createControlPanelElement("assgn nID", newID, new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {	
				String text = newID.getText();
				Node node = cPanel.getSelectedNode();
				conn.sendSerial(new NodeMessage("NRID|"+node.getNodeId() + ";"+text, 127));
				ePanel.addEvent("Sent connection request to gateway");
			}
		});
		
		JTextField nodeNameField = new JTextField();
		cPanel.createControlPanelElement("Set Name", nodeNameField, new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {					
				String text = nodeNameField.getText();
				//TODO: ensure text is an integer within minimum or maximum value
				Node node = cPanel.getSelectedNode();
				
				conn.sendSerial(new NodeMessage("SNM|"+ text, node.getNodeId()));
				ePanel.addEvent("Sent name to node " + node.getNodeId() + " with value: " + text);
			}
		});
		
		cPanel.createControlPanelElement("Get Name", new JTextField(), new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {	
				Node node = cPanel.getSelectedNode();
				conn.sendSerial(new NodeMessage("RNM|", node.getNodeId()));
				ePanel.addEvent("Sent name request to node");
			}
		});

		for(int i=1; i< 100;++i){
			cPanel.addNodeSelection(i);
		}
		return frmSensorNetwork;
	}
	
	public NodeControlTab initializeControlTab(){
		for(int i = 1; i<100; i++){
			nodes.put(i, new Node(i));	
		}
		return new NodeControlTab(nodes);
	}
	
	/*public DataProcessingTab initializeDataProcessingHandler(){
		return new DataProcessingTab(nodes);
	}
	
	public ActionsTab initializeActionTab(){
		return new ActionsTab(nodes);
	}*/
	
    public static void main(String[] args) {    
    	MNN_Host test = new MNN_Host();   	 	
    	test.initialize();		
    	
		System.out.println("Started");		
    }
    
    public void registerMessageHandler(NodeMessageHandler handler){
    	messageHandlers.put(handler.getMessageType().toLowerCase(), handler);
    }

	@Override
	//handle a parsed object
	public void onParsedObjectReady(ParsedObject o) {
	
		int from = o.getFrom();
		Node node = this.nodes.get(from);
		String msgType = o.getMsgType();
		NodeMessageHandler h =messageHandlers.get(msgType.toLowerCase());
		//see if this message type has a handler
		if(h!=null){
			h.handleMessage(node, from, o);
		//if not, see if there is a place for the message data
		}else{
			//see if the message can be found in the sendables
			Command c = node.getSendableCommand(msgType);
			if(c!=null){ //if it's found, parse the message and distribute the data
				//for each field in the command, distribute the data in the object
				if(node.hasName()){
					
					//System.out.println("1");
					DataItem item = new DataItem(o,c,node.getNodeName());			
					ThingSpeakPostAction tPoster = new ThingSpeakPostAction("3CPMVUR8K01PLVPB");
					tPoster.post(item.getThingSpeakString());
					
					//System.out.println("2");					
					JsonDataItem jItem = new JsonDataItem(o,c);
					XivelyPostAction xPoster = new XivelyPostAction("1760525797", "w31AdqfxCHdc9aUoiBUBOn4pRJ1hmLaxk4rQdCgTKeztlkDm");
					xPoster.post(jItem.getJsonString());
					
					//System.out.println("3");
				}
				
				for(CommandField cf: c.fields.values()){
					//System.out.println("Handling field1: " + cf.fieldname);
					TypedObject data = o.getFieldValue(cf.fieldname);
					if(data.type == ObjectType.STRING){
						//TODO: deal with fields that are strings
						cf.box.setText(data.getString());
					//data is a number, handle it
					}else{
						//System.out.println("Handling field2: " + cf.fieldname);
						Double val = data.getValue();
						cf.box.setText(val.toString());//update the node field value
						
						if(node.hasName()){
							String plotName = node.getNodeName()+"-"+c.getCommandString()+"-"+cf.fieldname;
							sPanel.updateFieldStatusValue(plotName, val);//update the status panel field value
							dataProcessingHandler.updateDataItem(plotName, val);
							//TODO: connect events to streams and update them when the stream 
							//is updated instead of here
							eventHandler.updateEventDataItem(node.getNodeName(), c.getCommandString(), cf.fieldname, val);
							graphs.addPoint(new FinePoint(Calendar.getInstance().getTimeInMillis(),val), plotName); //update the plot
						}else{
							String plotName = "Node"+from+"-"+c.getCommandString()+"-"+cf.fieldname;
							sPanel.updateFieldStatusValue(plotName, val);//update the status panel field value
							graphs.addPoint(new FinePoint(Calendar.getInstance().getTimeInMillis(),val), plotName); //update the plot
						}
					}
				}				
				eventHandler.updateHandlers();
			}			
		}//end else
	}//end on parsed object ready
}
