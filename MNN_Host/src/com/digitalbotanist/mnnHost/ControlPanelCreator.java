package com.digitalbotanist.mnnHost;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;


public class ControlPanelCreator {
	int yCurrent = 0;
	JPanel controlSubPanel = new JPanel();
	JComboBox nodeComboBox = new JComboBox();
	
	public void addNodeSelection(int nodeNum){
		nodeComboBox.addItem(new Node(nodeNum));		
	}
	
	public Node getSelectedNode(){
		return (Node) nodeComboBox.getSelectedItem();	
	}
	
	public ControlPanelCreator(Container parent){
		JPanel ControlPanel = new JPanel();
		ControlPanel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Controls", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagConstraints gbc_ControlPanel = new GridBagConstraints();
		gbc_ControlPanel.fill = GridBagConstraints.BOTH;
		gbc_ControlPanel.gridx = 1;
		gbc_ControlPanel.gridy = 1;
		
		parent.add(ControlPanel, gbc_ControlPanel);
		ControlPanel.setLayout(new BorderLayout(0, 0));
		
		JScrollPane scrollPane_1 = new JScrollPane();
		ControlPanel.add(scrollPane_1, BorderLayout.CENTER);
		
		controlSubPanel.setForeground(Color.WHITE);
		controlSubPanel.setBorder(new EmptyBorder(2, 2, 2, 2));
		scrollPane_1.setViewportView(controlSubPanel);
		GridBagLayout gbl_controlSubPanel = new GridBagLayout();
		gbl_controlSubPanel.columnWidths = new int[] {0, 0, 0, 0};
		//TODO:  row heights and weights need to be updated if adding more than 15 elements
		//see status panel code (createStatusPanelElement) if more flexibility is needed
		gbl_controlSubPanel.rowHeights = new int[]{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
		gbl_controlSubPanel.columnWeights = new double[]{0.0, 1.0, 0.0, Double.MIN_VALUE};
		gbl_controlSubPanel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0,Double.MIN_VALUE};
		controlSubPanel.setLayout(gbl_controlSubPanel);
		
		//add the node selector label and combobox
		JLabel lblNode = new JLabel("Node");
		GridBagConstraints gbc_lblNode = new GridBagConstraints();
		gbc_lblNode.anchor = GridBagConstraints.WEST;
		gbc_lblNode.insets = new Insets(0, 0, 5, 5);
		gbc_lblNode.gridx = 0;
		gbc_lblNode.gridy = 0;
		controlSubPanel.add(lblNode, gbc_lblNode);
		
		
		GridBagConstraints gbc_comboBox_1 = new GridBagConstraints();
		gbc_comboBox_1.gridwidth = 2;
		gbc_comboBox_1.insets = new Insets(0, 0, 5, 5);
		gbc_comboBox_1.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBox_1.gridx = 1;
		gbc_comboBox_1.gridy = 0;
		controlSubPanel.add(nodeComboBox, gbc_comboBox_1);
		yCurrent=1;
	}
	
	public void createControlPanelElement(String label, JTextField textField, ActionListener listener){
		//control panel element
		JLabel lblReportInterval = new JLabel(label);
		GridBagConstraints gbc_lblReportInterval = new GridBagConstraints();
		gbc_lblReportInterval.insets = new Insets(0, 0, 0, 5);
		gbc_lblReportInterval.anchor = GridBagConstraints.WEST;
		gbc_lblReportInterval.gridx = 0;
		gbc_lblReportInterval.gridy = yCurrent;
		controlSubPanel.add(lblReportInterval, gbc_lblReportInterval);
		
		GridBagConstraints gbc_textField = new GridBagConstraints();
		gbc_textField.insets = new Insets(0, 0, 0, 5);
		gbc_textField.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField.gridx = 1;
		gbc_textField.gridy = yCurrent;
		controlSubPanel.add(textField, gbc_textField);
		textField.setColumns(10);
				
		GridBagConstraints gbc_btnNewButton = new GridBagConstraints();
		gbc_btnNewButton.gridx = 2;
		gbc_btnNewButton.gridy = yCurrent;
		JButton sendButton = new JButton("Send");
		sendButton.addActionListener(listener);
		controlSubPanel.add(sendButton, gbc_btnNewButton);
		yCurrent++;
	}
	
}
