package com.digitalbotanist.mnnHost.NodeMessageHandler;

import com.digitalbotanist.mnnHost.Node;
import com.digitalbotanist.mnnHost.serial.ParsedObject;
import com.digitalbotanist.mnnHost.serial.SerialConnector;

public abstract class NodeMessageHandler {
	private String messageType;
	protected SerialConnector conn;
	
	NodeMessageHandler(String messageType){
		this.messageType = messageType;
		conn = SerialConnector.getInstance();
	}
	
	public String getMessageType(){
		return messageType;
	}
	
	public abstract void handleMessage(Node node, int nodeId, ParsedObject o);
}
