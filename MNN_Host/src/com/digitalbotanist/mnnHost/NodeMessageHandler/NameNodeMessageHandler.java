package com.digitalbotanist.mnnHost.NodeMessageHandler;
import java.util.HashMap;

import com.digitalbotanist.mnnHost.Node;
import com.digitalbotanist.mnnHost.serial.ParsedObject;


public class NameNodeMessageHandler extends NodeMessageHandler {

	private HashMap<String, Integer> nodeNames = new HashMap<String, Integer>();
	
	public NameNodeMessageHandler() {
		super("nam");
	}

	@Override
	public void handleMessage(Node node, int from, ParsedObject o) {
		String name = o.getFieldValue("v").getString();			
		Integer nodeId = nodeNames.get(name);			
		System.out.println(System.currentTimeMillis() + " - Received name from node " + from + ": " + name);
			
		if(nodeId!=null){
			//TODO: the node name is already in the network node name list - resolve any conflicts 
			//move the node data from this node id to the new nodeId
			
			//update the nodename/id connection
			nodeNames.put(name, from);
			//clear the data from the old node id
			node.clearCommands();
			//set the node's new name
			node.setNodeName(name);	
		
		}else{
			//the node is not in the network node name list, add it
			nodeNames.put(name, from);
			node.setNodeName(name);
		}
	}

}
