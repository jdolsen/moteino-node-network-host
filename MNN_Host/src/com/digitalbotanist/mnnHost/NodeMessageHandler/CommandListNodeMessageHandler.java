package com.digitalbotanist.mnnHost.NodeMessageHandler;

import com.digitalbotanist.mnnHost.Node;
import com.digitalbotanist.mnnHost.serial.NodeMessage;
import com.digitalbotanist.mnnHost.serial.ParsedObject;
import com.digitalbotanist.mnnHost.serial.TypedObject;

public class CommandListNodeMessageHandler extends NodeMessageHandler {

	public CommandListNodeMessageHandler() {
		super("cf");
	}

	@Override
	public void handleMessage(Node node, int from, ParsedObject o) {
		//indicates the commands that the node will accept or send
		TypedObject cmd = o.getFieldValue("cmds");
		TypedObject snd = o.getFieldValue("snds");

		
		if(cmd!=null){
			String[]commandsStrings = cmd.getString().split(",");					
			for(String command: commandsStrings){
				System.out.println(System.currentTimeMillis() + " - Node: " + from + " accepts command: " + command);
				if(node.addReceivableCommand(command)){								
					NodeMessage mssg = new NodeMessage("FRQ|cmd:" + command, from, "acf", 250);
					conn.sendSerial(mssg);
				}
			}				
		}
		
		if(snd!=null){
			String[]sendStrings = snd.getString().split(",");
			for(String message: sendStrings){
				System.out.println(System.currentTimeMillis() + " - Node: " + from + " sends message: " + message);
				if(node.addSendableCommand(message)){	
					NodeMessage mssg = new NodeMessage("FRQ|cmd:" + message, from, "ACF", 250);
					conn.sendSerial(mssg);
				}
			}
		}
		
		node.printNode();
	}

}
