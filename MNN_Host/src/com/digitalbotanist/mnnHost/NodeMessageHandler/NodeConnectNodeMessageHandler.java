package com.digitalbotanist.mnnHost.NodeMessageHandler;

import com.digitalbotanist.mnnHost.Node;
import com.digitalbotanist.mnnHost.serial.NodeMessage;
import com.digitalbotanist.mnnHost.serial.ParsedObject;

public class NodeConnectNodeMessageHandler extends NodeMessageHandler {

	public NodeConnectNodeMessageHandler() {
		super("nodeconnect");

	}

	@Override
	public void handleMessage(Node node, int from, ParsedObject o) {
		System.out.println(from + " has connected");	
		node.resetNode();
		node.setStatus(o.getFieldValue("NS").getValue().intValue());
		conn.sendSerial(new NodeMessage("RNM|", from, "NAM"));
		conn.sendSerial(new NodeMessage("RCMD|", from, "CF"));//o.getFrom() + "|RCMD|", o.getFrom(), 0);//send a request for the node's commands
	}

}
