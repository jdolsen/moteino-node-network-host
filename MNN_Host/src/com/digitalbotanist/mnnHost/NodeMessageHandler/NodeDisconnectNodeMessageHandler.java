package com.digitalbotanist.mnnHost.NodeMessageHandler;

import com.digitalbotanist.mnnHost.Node;
import com.digitalbotanist.mnnHost.serial.ParsedObject;

public class NodeDisconnectNodeMessageHandler extends NodeMessageHandler {

	public NodeDisconnectNodeMessageHandler() {
		super("nodedisconnect");
	}

	@Override
	public void handleMessage(Node node, int from, ParsedObject o) {
		System.out.println(from + " has disconnected");
		//clear messages from the message queue
		conn.nodeDisconnect(from);
		node.resetNode();
		node.setStatus(o.getFieldValue("NS").getValue().intValue());		
	}

}
