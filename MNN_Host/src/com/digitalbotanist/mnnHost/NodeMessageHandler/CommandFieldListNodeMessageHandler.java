package com.digitalbotanist.mnnHost.NodeMessageHandler;

import com.digitalbotanist.mnnHost.Command;
import com.digitalbotanist.mnnHost.Node;
import com.digitalbotanist.mnnHost.MNN_Host;
import com.digitalbotanist.mnnHost.serial.ParsedObject;

public class CommandFieldListNodeMessageHandler extends NodeMessageHandler {

	//TODO: move the graphing stuff into a display object.
	public MNN_Host parent;
	
	public CommandFieldListNodeMessageHandler(MNN_Host parent) {
		super("acf");
		this.parent = parent;
	}

	@Override
	public void handleMessage(Node node, int from, ParsedObject o) {
		System.out.println(System.currentTimeMillis() + " - Received ACF from node " + from + " for command: " + o.getFieldValue("cmd").getString() +
				" with fields: " + o.getFieldValue("f").getString());
		
		String command = o.getFieldValue("cmd").getString();
		String[] fields = o.getFieldValue("f").getString().split(",");
		
		for(String field: fields){

			boolean sendable = false;
			Command nodeCmd = node.getReceivableCommand(command);
			if(nodeCmd == null){		
				System.out.println("Command was not receivable.  Trying sendable");
				nodeCmd = node.getSendableCommand(command);
				sendable = true;
			}
			
			if(nodeCmd!=null){
				System.out.println("Found Command: " + nodeCmd.getCommandString());// + " adding field: " + field);
				nodeCmd.addField(field);
				if(sendable){
					String plotName = null;
					if(node.hasName()){
						plotName = node.getNodeName()+"-"+nodeCmd.getCommandString()+"-"+field;
					}else{
						plotName = "Node"+from+"-"+nodeCmd.getCommandString()+"-"+field;
					}
					
					parent.addNewGraph(plotName);//, from, nodeCmd.commandString, field);
					//TODO add this node/command/field combo for selection on the events tab
				}
			}else{
				System.out.println("Could not find command: " + command);
			}
		}
	}
}
