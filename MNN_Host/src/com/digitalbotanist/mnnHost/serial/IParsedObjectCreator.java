package com.digitalbotanist.mnnHost.serial;

public interface IParsedObjectCreator {
	public ParsedObject parse(String input);
}
