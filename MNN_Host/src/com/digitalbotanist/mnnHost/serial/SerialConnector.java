package com.digitalbotanist.mnnHost.serial;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import javax.swing.JMenu;
import javax.swing.JMenuItem;

import com.digitalbotanist.mnnHost.Node;

import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent; 
import gnu.io.SerialPortEventListener; 

public class SerialConnector implements SerialPortEventListener, Runnable{

	private static final String PORT_NAMES[] = { 
		"/dev/tty.usbserial-A9007UX1", // Mac OS X
		"/dev/ttyUSB0", // Linux
		"COM3", // Windows
		"COM4",//, // Windows
		"COM5",  // Windows
		"COM17"
	};
	private static SerialConnector instance;
	SerialPort serialPort;
	private BufferedReader input;
	private OutputStream output;
	private IParsedObjectReceiver rx;
	private IParsedObjectCreator parser;
	private static final int MIN_DELAY_BETWEEN_MESSAGES = 50;
	private static final int TIME_OUT = 2000; //ms to wait for port to open/
	private static final int DATA_RATE = 9600; //bps for comm port
	private CommPortIdentifier connectedPort = null;
	HashMap<Integer, ArrayList<NodeMessage>> nodeMessages = new HashMap<Integer, ArrayList<NodeMessage>>();
	HashMap<Integer, ScheduledFuture<?>> queuedMessages = new HashMap<Integer, ScheduledFuture<?>>();
	private boolean gatewayAck = false;
	//ExecutorService executor = Executors.newSingleThreadExecutor();
	ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
	private HashMap<Integer, Node> nodes;
	
	
	//---------------------------------------------------------------------------------

	private SerialConnector(){//IParsedObjectReceiver rx, IParsedObjectCreator p){
		//this.rx = rx;
		//this.parser = p;
		for(int i = 0; i < 128; ++i){
			nodeMessages.put(i, new ArrayList<NodeMessage>());
		}
	}
	
	public static SerialConnector getInstance(){
		if(instance==null){
			instance = new SerialConnector();
		}		
		return instance;
	}
	
	//---------------------------------------------------------------------------------
	
	public void initialize(IParsedObjectReceiver rx, IParsedObjectCreator p, HashMap<Integer, Node> nodes){
		this.rx = rx;
		this.parser = p;
		this.nodes = nodes;
	}
	
	//---------------------------------------------------------------------------------
	
//	public static String getFriendlyName(String registryKey) {
//	    if (registryKey == null || registryKey.isEmpty()) {
//	        throw new IllegalArgumentException("'registryKey' null or empty");
//	    }
//	    try {
//	        int hkey = WinRegistry.HKEY_LOCAL_MACHINE;
//	        return WinRegistry.readString(hkey, registryKey, "FriendlyName");
//	    } catch (Exception ex) { // catch-all: 
//	        // readString() throws IllegalArg, IllegalAccess, InvocationTarget
//	        System.err.println(ex.getMessage());
//	        return null;
//	    }
//	}
	
	//---------------------------------------------------------------------------------

	public void connect(){
		CommPortIdentifier portId = null;
		Enumeration portEnum = CommPortIdentifier.getPortIdentifiers();

		//First, Find an instance of serial port as set in PORT_NAMES.
		while (portEnum.hasMoreElements()) {
			CommPortIdentifier currPortId = (CommPortIdentifier) portEnum.nextElement();
			
			System.out.println("Found comm port: " + currPortId.getName() + 
					" with owner: " + currPortId.getCurrentOwner() + 
					" and type: " + currPortId.getPortType() +
					" and is owned: " + currPortId.isCurrentlyOwned());
			
			//search registry for friendly name:			
			for (String portName : PORT_NAMES) {
				//TODO: do some more thorough handshaking to find the gateway
				if (currPortId.getName().equals(portName)) {
					portId = currPortId;
					break;
				}
			}
		}
		
		//if none is found return null
		if (portId == null) {
			System.out.println("Could not find COM port.");//TASK:  change this to throw an exception
			return;
		}
		
		connectToPort(portId);
	}
		
	private void connectToPort(CommPortIdentifier portId){
		if(portId == connectedPort){
			System.out.println("Already connected");
		}else{
		try {	
			connectedPort = portId;
			// open serial port, and use class name for the appName.
			serialPort = (SerialPort) portId.open(this.getClass().getName(), TIME_OUT);
			serialPort.getName();
			// set port parameters
			serialPort.setSerialPortParams(DATA_RATE, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);

			// open the streams
			input = new BufferedReader(new InputStreamReader(serialPort.getInputStream()));
			output = serialPort.getOutputStream();

			// add event listener
			serialPort.addEventListener(this);
			serialPort.notifyOnDataAvailable(true);
			System.out.println("Connected to port: " + serialPort.getName());
			new Thread(this).start();
			
			//TODO: handshake the new port!  Fail if no device is found
			sendSerial(new NodeMessage("CNREQ|" +99, 127));
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		}
	}
	
	public void disconnectCurrentPort(){
		serialPort.close();			
	}

	private void resolveStatus(ParsedObject obj){
		String[] nodeStatStrings = obj.getFieldValue("Nodes").getString().split(",");
		System.out.println(nodeStatStrings.length + " Nodes reported on");
		for(int i = 0; i<100; ++i){
			int status = Integer.parseInt(nodeStatStrings[i]);
			if(status < 3){
				//request the name of the node and its commands
				System.out.println(System.currentTimeMillis() + " - Requesting name and commands from node: " + (i + 1));				
				sendSerial(new NodeMessage("RNM|", i+1, "NAM"));
				sendSerial(new NodeMessage("RCMD|", i+1, "CF"));
			}
			nodes.get(i+1).setStatus(status);
		}		
	}
	
	//---------------------------------------------------------------------------------
	
	@Override
	public void serialEvent(SerialPortEvent sEvent) {
		if (sEvent.getEventType() == SerialPortEvent.DATA_AVAILABLE) {
			try {
				//create a parsed object from the recieved serial data
				ParsedObject obj = parser.parse(input.readLine());					
				
				if(obj !=null){
					if(obj.getMsgType().equalsIgnoreCase("GWS")){
						resolveStatus(obj);
						return;
					}
					
					if(obj.getMsgType().equalsIgnoreCase("GWACK")){						
						gatewayAck = true;
					}
					//TASK: create a new class to hold all this data exchange stuff...
					System.out.println(System.currentTimeMillis() + " - Received Message from node: " + obj.getFrom() + " - "+ obj.getMsgType());
					//determine if this message satisfies a wait condition.
					//if it does, remove it.  If there are more messages for this node,
					//prepare the next one to be sent
					//System.out.println("Retrieving ArrayList for node: " + obj.getFrom());
					synchronized(this){
						ArrayList<NodeMessage> messages = nodeMessages.get(obj.getFrom());	
						if(messages!=null && !messages.isEmpty()){
							//System.out.println("ArrayList contains a message");
							NodeMessage mssg = messages.get(0);
							//System.out.println("Message type received is: " + obj.getMsgType());
							if(mssg.isResponseConditionMet(obj.getMsgType())){
								System.out.println("responseConditions are met");
								//the desired response has just been received, remove the
								//message from the list and prepare the next to be sent
								messages.remove(0);
								nodes.get(obj.getFrom()).setMessageSuccess();
								sendFirstMessage(messages, true);
							}else{
								//System.out.println("responseConditions are not met");
							}
						}
					}
					//send the message to the message receiver
					rx.onParsedObjectReady(obj);	
				}
			}catch(IOException e){				
			}
		}		
	}
	
	//---------------------------------------------------------------------------------
	
	public boolean sendSerial(NodeMessage mssg){//String data, int dest, int delay){

		System.out.println(System.currentTimeMillis() + " - Adding new serial message for node: " + mssg.destNode + " -> " + mssg.message);
		ArrayList<NodeMessage> messages = nodeMessages.get(mssg.destNode);
		Boolean isEmpty = messages.isEmpty();
		
		messages.add(mssg);
		if(isEmpty){
			sendFirstMessage(messages, true);
		}
				
		return true;
	}
	
	public void schedulePeriodicMessage(NodeMessage mssg, int periodMs){
		System.out.println(System.currentTimeMillis() + " - Adding new periodic message for node: " + mssg.destNode + " -> " + mssg.message);

		mssg.delay = 0; //the scheduler will handle the message delay		
		executor.scheduleAtFixedRate(new NodeMessageSender(mssg, this){
			@Override
			public void run() {
				
				sendSerial(mssg);					
			}	
		//no initial delay periodMs is the period, unit is in milliseconds
		}, 0, periodMs, TimeUnit.MILLISECONDS);
	}
	
	//---------------------------------------------------------------------------------
	
	public synchronized void close() {
		if (serialPort != null) {
			serialPort.removeEventListener();
			serialPort.close();
		}
	}
	
	//---------------------------------------------------------------------------------
	
	//every 250ms go through the list to see if there are any messages that have timed out -
	//the message response criteria has not been met, retry (or error out if reached max retries)
	//only one message will be in a sendable or resendable state for any single node
	public void run(){

		while(true){
			try {
				Thread.sleep(250);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
			
			synchronized(this){//make sure there are no removals from the list while checking it
				Set<Entry<Integer, ArrayList<NodeMessage>>> nodeMessageSet = nodeMessages.entrySet();
				long currentTime = System.currentTimeMillis();
				
				//inspect the first message of every node
				for(Entry<Integer, ArrayList<NodeMessage>> e:nodeMessageSet){
					int nodeId = e.getKey();
					ArrayList<NodeMessage> messages = e.getValue();
					if(!messages.isEmpty()){
						NodeMessage message = messages.get(0);
						//if the message is waiting for a response and has timed out
						if(message.isWaitingForResponse() &&
						   (currentTime - message.sentTime >= NodeMessage.TIMEOUT_MS)){
							if(message.numRetries < NodeMessage.MAX_RETRIES){							
								message.numRetries++;
								//retry sending the message with no delay
								System.out.println(System.currentTimeMillis() + " - Retrying message to node: " + nodeId);
								sendFirstMessage(messages, false);
							}else{
								System.out.println(System.currentTimeMillis() + " - Message Error! For message to node: " + nodeId);
								nodes.get(nodeId).setMessageFailed();
								messages.remove(0);
								sendFirstMessage(messages, true);
							}												
						}
					}
				}//end for
			}
		}//end while
	}//end run
	
	//---------------------------------------------------------------------------------
	
	public void sendFirstMessage(ArrayList<NodeMessage> messages, boolean delay){
		if(!messages.isEmpty()){
			NodeMessage message = messages.get(0);

			/*int delayVal = 0;
			if(delay){
				delayVal = message.delay;
			}*/
			
			if(message.delay < 100){
				message.delay = 100;
			}
			
			message.setReadyToSend();
			
			ScheduledFuture<?> task = executor.schedule(new NodeMessageSender(message, this){
				@Override
				public void run() {
					message.setWaitingForResponse();
					message.sentTime = System.currentTimeMillis();
					System.out.println(System.currentTimeMillis() + " - Sending message via scheduler: " + message.message);
					try{	
						setWaitingForGatewayAck();
						output.write(message.message.getBytes());
						output.write('\0');
						output.flush();		
						queuedMessages.remove(message.destNode);
						waitForGatewayAck(message.destNode);						
					} catch (IOException e) {	
						e.printStackTrace();
					}
				
				}								
			}, message.delay, TimeUnit.MILLISECONDS);
			
			queuedMessages.put(message.destNode, task);

		}//end if not empty
	}

	public void addComSelections(JMenu gwConnect) {
		Enumeration portEnum = CommPortIdentifier.getPortIdentifiers();				
		while (portEnum.hasMoreElements()) {
			CommPortIdentifier currPortId = (CommPortIdentifier) portEnum.nextElement();			
			System.out.println("Found comm port: " + currPortId.getName() + 
					" with owner: " + currPortId.getCurrentOwner() + 
					" and type: " + currPortId.getPortType() +
					" and is owned: " + currPortId.isCurrentlyOwned());
			
			//create a menu item for the port
			if(currPortId.getName().contains("COM")){
				JMenuItem comOption= new JMenuItem(currPortId.getName());
				comOption.addActionListener(new ActionListener(){
					public void actionPerformed(ActionEvent arg0) {
						System.out.println(currPortId.getName() + " selected");
						connectToPort(currPortId);
					}				
				});
				gwConnect.add(comOption);
			}//end if
		}//end while
	}
	
	//waits up to 1 second for an ack from the gateway
	synchronized public void waitForGatewayAck(int node){		
		int i = 0;
		while(!gatewayAck && i < 100){
			//System.out.println(System.currentTimeMillis() + " - Node " + node + " waiting for Gateway Ack");
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			++i;
		}
	}
	
	synchronized private void setWaitingForGatewayAck(){
		gatewayAck = false;
	}

	synchronized public void nodeDisconnect(int node) {
		//clear waiting messages for the node
		nodeMessages.get(node).clear();
		
		//clear any messages being sent 
		ScheduledFuture<?> task = queuedMessages.get(node);
		if(task!=null){
			task.cancel(false);
		}
		queuedMessages.remove(node);
	}
	
}//end class
