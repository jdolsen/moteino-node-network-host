package com.digitalbotanist.mnnHost.serial;
import java.util.HashMap;


public class ParsedObject {
	private HashMap<String, TypedObject> fields = new HashMap<String, TypedObject>();
	private String msgType;
	private int from;
	private int to;
	
	public ParsedObject(String msgType, int from, int to){
		this.msgType = msgType;
		this.addField("MessageType", msgType);
		this.from = from;
		this.to = to;
	}
	
	
	public void addField(String name, Double value){
		fields.put(name, new TypedObject(value));
	}
	
	public void addField(String name, String value){
		fields.put(name, new TypedObject(value));
	}
	
	public TypedObject getFieldValue(String fieldName){
		return fields.get(fieldName);
	}
	
	public HashMap<String, TypedObject> getFields(){
		return fields;
	}


	public String getMsgType() {
		return msgType;
	}


	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}


	public int getFrom() {
		return from;
	}


	public void setFrom(int from) {
		this.from = from;
	}


	public int getTo() {
		return to;
	}


	public void setTo(int to) {
		this.to = to;
	}
	
	
}
