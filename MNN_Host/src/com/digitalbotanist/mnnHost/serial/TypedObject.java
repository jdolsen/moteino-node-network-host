package com.digitalbotanist.mnnHost.serial;


public class TypedObject {
	public ObjectType type;
	Object value;
	
	TypedObject(String value){
		this.value = value;
		type = ObjectType.STRING;
	}
	
	TypedObject(Double value){
		this.value = value;
		type = ObjectType.DOUBLE;
	}
	
	public ObjectType getType() {
		return type;
	}
	public void setType(ObjectType type) {
		this.type = type;
	}
	public Double getValue() {
		if(type==ObjectType.DOUBLE){
			return (Double)value;
		}else{
			return Double.NaN;
		}
	}
	
	public String getString(){
		if(type==ObjectType.DOUBLE){
			return ((Double)value).toString();
		}else{
			return ((String)value);
		}
	}
	
	public void setValue(Object value) {
		this.value = value;
	}
	
	
}
