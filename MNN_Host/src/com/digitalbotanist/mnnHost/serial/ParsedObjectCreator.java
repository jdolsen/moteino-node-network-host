package com.digitalbotanist.mnnHost.serial;
import java.awt.Color;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;

import com.digitalbotanist.mnnHost.EventPanelCreator;


public class ParsedObjectCreator implements IParsedObjectCreator {

	int GATEWAY_ID = 127;
	String SECTION_SEPARATOR = "\\|";
	//String ERROR_SECTION_SEPARATOR = "!";
	String FIELD_SEPARATOR = ";";
	String LABEL_SEPARATOR = ":";
	String GATEWAY_INFO_STR = "GWI:";
	String GATEWAY_ERROR_STR = "GWER:";
	
	String NODE_CONNECT_EVENT = "NCE:";
	String NODE_DISCONNECT_EVENT = "NDE:";
	private EventPanelCreator events;
	
	//public ParsedObjectCreator(){};
	
	public ParsedObjectCreator(EventPanelCreator events){
		this.events = events;
	};
	
	@Override
	public ParsedObject parse(String input) {

		try{
			String mssg = input.split("\0")[0];//only take the part of the string before the first null character		
			
			//System.out.println("Message: " + mssg);
			//Check for gateway info
			if(input.startsWith(GATEWAY_INFO_STR)){				
				String[] tokens = mssg.split(GATEWAY_INFO_STR);
				String gwMssg = tokens[1];
				printGWEvent("Gateway: " + gwMssg);
				return null;
			}
			
			//check for gateway error
			if(input.startsWith(GATEWAY_ERROR_STR)){				
				String[] tokens = mssg.split(GATEWAY_ERROR_STR);
				String gwMssg = tokens[1];
				printError("Gateway: " + gwMssg);
				return null;
			}	
			
			//check for gateway status message
			if(input.startsWith("GWS")){				
				//String[] tokens = mssg.split(GATEWAY_ERROR_STR);
				//String gwMssg = tokens[1];
				printGWEvent("Gateway: " + mssg);
				ParsedObject obj = new ParsedObject("GWS", 127, 127);
				String[] tokens = mssg.split(SECTION_SEPARATOR);
				String msgData = tokens[1];//.trim().split(FIELD_SEPARATOR);
				
				StringBuilder fields = parseMessageData(msgData, obj);					
				printInfo("GWS Message received from node: 127 with fields:" + fields.toString());
				
				return obj;
			}	
			
			//check for node connection
			if(input.startsWith(NODE_CONNECT_EVENT)){		
				String gwMssg = mssg.split(NODE_CONNECT_EVENT)[1];
				printNodeEvent("Node Connect: " + gwMssg);
				String[] data = gwMssg.split(SECTION_SEPARATOR);
				String nodeNum = data[0];
				String mssgParams = data[1];
				int node = Integer.parseInt(nodeNum);							
				ParsedObject obj = new ParsedObject("nodeConnect", node, node);				
				parseMessageData(mssgParams, obj);
				return obj;
			}	
			
			//check for node disconnection
			if(input.startsWith(NODE_DISCONNECT_EVENT)){	
				String gwMssg = mssg.split(NODE_DISCONNECT_EVENT)[1];
				printNodeEvent("Node Disconnect: " + gwMssg);
				String[] data = gwMssg.split(SECTION_SEPARATOR);				
				String nodeNum = data[0];
				String mssgParams = data[1];
				int node = Integer.parseInt(nodeNum);
				ParsedObject obj = new ParsedObject("nodeDisconnect", node, node);
				parseMessageData(mssgParams, obj);
				return obj;
			}	
			
			//Process any other message type			
			String[] tokens = mssg.split(SECTION_SEPARATOR);
				
			//make sure the message length at least the minimum size
			if(tokens.length < 4){
				printError("Unknown message: " + input);
				return null;
			}		
			
			String from = tokens[0];
			String to = tokens[1];
			String msgType = tokens[2];
			String msgData = tokens[3];
			ParsedObject obj = new ParsedObject(msgType, Integer.parseInt(from), Integer.parseInt(to));
						
			StringBuilder fields = parseMessageData(msgData, obj);					
			printInfo(msgType + " Message received from node: " + from + " with fields:" + fields.toString());				
										
			return obj;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	StringBuilder parseMessageData(String msgParams, ParsedObject obj){
		String[] msgData = msgParams.trim().split(FIELD_SEPARATOR);
		StringBuilder fields = new StringBuilder();
		int i = 0;
		for (String s : msgData){
			String values[] = s.split(LABEL_SEPARATOR);
			
			String fieldId = null;
			String fieldVal = null;
			
			if(values.length > 1){
				fieldId = values[0];
				fieldVal = values[1];
			}else{
				fieldId = "val" + i;
				fieldVal = values[0];					
			}
			fields.append(" " + fieldId + ":" + fieldVal);
			try{
				Double fieldValD = Double.parseDouble(fieldVal);
				obj.addField(fieldId, fieldValD);
			}catch(NumberFormatException e){
				obj.addField(fieldId, fieldVal); //when the field value is not a number
			}
			++i;
		}	
		return fields;
	}
	
	void print(String s, Color c){
		if(events == null){
			System.out.println("null event panel");
		}else{
			events.addEvent(s, c);
		}
	}
	
	void printError(String s){
		print(s, Color.getHSBColor(41, 97, 96));//Color.ORANGE);
		
	}
	
	void printInfo(String s){
		print(s, Color.GRAY);
	}
	
	void printEvent(String s){
		print(s, Color.GREEN);
	}
	
	void printGWEvent(String s){
		print(s, Color.GREEN.darker().darker().darker());
	}
	
	void printNodeEvent(String s){
		print(s, Color.BLUE);		
	}

}
