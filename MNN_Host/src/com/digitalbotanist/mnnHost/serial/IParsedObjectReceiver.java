package com.digitalbotanist.mnnHost.serial;

public interface IParsedObjectReceiver {
	public void onParsedObjectReady(ParsedObject o);
}
