package com.digitalbotanist.mnnHost.serial;

public class NodeMessage {
	
	public enum MessageState{NONE, READY_TO_SEND, WAITING};
	
	static final int MAX_RETRIES = 5; //retry up to 5 times before failing
	static final int TIMEOUT_MS = 10000;//time out a message and retry after 20 seconds
	static final String ANY_NODE_MESSAGE = "ANY_NODE_MESSAGE_POSSIBLE_BUT_NOT_GWACK_RESPONSES_FROM_THE_GATEWAY_THOSE_ARE_INDICATED_BY_THE_GATEWAY_RESPONSE_MESSAGE_STRING_BELOW_WHICH_IS_GWACK";
	static final String GWACK = "GWACK";
	
	public String message;
	public int delay = 0; //minimum time to wait between sending
					  //messages to this node
	//public long startTime;//record this time when the previous 
		//message is complete and add this to the executor
		//if the current system time is equal or greater than the
		//startTime plus delay values then the message will be sent
		//otherwise the system will wait to send the message
	public long sentTime = 0;
	public int numRetries = 0;
	public int destNode = 0;
	public MessageState state = MessageState.NONE;
	private String waitForResponseType = GWACK; //holds a string of the message type this message will
	public String messageType;
									//wait for before continuing to the next message - default is "GWACK"
									// - the gateway acknowledgement returned after the message is sent
	
	public NodeMessage(String mssg, int destination){
		setMessageType(mssg);
		this.message = destination + "|" + mssg;
		this.destNode = destination;
		this.delay = 0;
	}

	public NodeMessage(String mssg, int destination, int delayMs){
		setMessageType(mssg);
		this.message = destination + "|" + mssg;
		this.destNode = destination;
		this.delay = delayMs;
	}
	
	public NodeMessage(String mssg, int destination, String waitForMssg) {
		setMessageType(mssg);
		this.message = destination + "|" + mssg;
		this.destNode = destination;
		this.delay = 0;		
		waitForResponseType = waitForMssg;
	}
	
	public NodeMessage(String mssg, int destination, String waitForMssg, int delayMs) {
		setMessageType(mssg);
		this.message = destination + "|" + mssg;
		this.destNode = destination;
		this.delay = delayMs;		
		waitForResponseType = waitForMssg;
	}

	private void setMessageType(String mssg) {
		String[] tokens = mssg.split("\\|");
		this.messageType = tokens[0];
		
	}
	
	public void setDelay(int delayMs){
		this.delay = delayMs;
	}
	
	public void setWaitForResponseMessage(String mssgType){
		if(mssgType!=null){
			waitForResponseType=mssgType;
		}
	}
	
	public boolean isResponseConditionMet(String response){
		//if this message has not been sent yet, its state is NONE or SENDING
		//only when the message is WAITING for a response can a response condition be met
		//System.out.println("wait type is: " + waitForResponseType + " response type is: " + response + " returning: " + isWaitingForResponse());
		if(waitForResponseType.equalsIgnoreCase(GWACK)){
			//if the waitforresponsetype is GWACK, return isrunning for any response regarding this node (or gwack)
			return isWaitingForResponse();
		}else if(waitForResponseType.equalsIgnoreCase(ANY_NODE_MESSAGE) && !response.equalsIgnoreCase(GWACK)){
			//return isrunning since a message was received from the node (but not a GWACK message from the gateway)
			return isWaitingForResponse();
		}else{
			//return isrunning if the response is the same as the value of waitforresponsetype 
			return isWaitingForResponse() && waitForResponseType.equalsIgnoreCase(response);
		}		
	}
	
	public boolean isWaitingForResponse(){
		return state==MessageState.WAITING;
	}
	
	public boolean isQueued(){
		return state==MessageState.NONE;
	}

	public void setReadyToSend() {
		state=MessageState.READY_TO_SEND;		
	}
	
	public void setWaitingForResponse(){
		System.out.println(System.currentTimeMillis() + " - Message is waiting for response");
		state=MessageState.WAITING;
	}
	
	
	
}
