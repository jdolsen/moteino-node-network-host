package com.digitalbotanist.mnnHost.serial;

import java.util.HashMap;
import java.util.Map.Entry;

import com.digitalbotanist.mnnHost.Command;
import com.digitalbotanist.mnnHost.CommandField;

public class DataItem {
	public String nodeName;
	public String command;
	public HashMap<String, Double> fields = new HashMap<String, Double>();
	
	public DataItem(ParsedObject o, Command c, String name){
		nodeName = name;
		command = c.commandString;
		for(CommandField cf: c.fields.values()){
			fields.put(cf.fieldname, o.getFieldValue(cf.fieldname).getValue());
		}
	}
	
	public String getString(){
		StringBuilder s = new StringBuilder();
		for(Entry e:fields.entrySet()){
			s.append(e.getKey());		
			s.append("=");
			s.append(e.getValue().toString());
			s.append("&");	
		}
		return s.toString();
		//System.out.println(s.toString());
	}
	
	public String getThingSpeakString(){
		StringBuilder s = new StringBuilder();
		int i=1;
		for(Entry e:fields.entrySet()){
			s.append("field");
			s.append(i++);
			s.append("=");
			s.append(e.getValue().toString());
			s.append("&");
			
			
		}
		
		s.deleteCharAt(s.length()-1);
		
		return s.toString();
		//System.out.println(s.toString());
	}
	
	
	
	
	
	
}
