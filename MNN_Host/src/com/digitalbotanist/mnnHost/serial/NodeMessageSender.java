package com.digitalbotanist.mnnHost.serial;
import gnu.io.SerialPortEventListener;


public abstract class NodeMessageSender implements Runnable {
	
	private NodeMessage message;
	private SerialConnector conn;
	
	NodeMessageSender(NodeMessage mssg, SerialConnector connector){
		this.message = mssg;
		this.conn = connector;
	}


}
