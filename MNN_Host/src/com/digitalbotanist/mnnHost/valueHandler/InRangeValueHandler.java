package com.digitalbotanist.mnnHost.valueHandler;

import com.digitalbotanist.mnnHost.events.NodeEvent;

public class InRangeValueHandler extends ValueHandler {

	double highVal;
	double lowVal;
	
	public InRangeValueHandler(String id, double highVal, double lowVal, NodeEvent event) {
		super(id, event, "In Range", "In Range " + highVal + " and " + lowVal);
		this.highVal = highVal;
		this.lowVal = lowVal;
	}

	@Override
	public boolean valueTriggersEvent(double val) {
		if(val<=highVal && val >=lowVal){
			return true;
		}
		return false;
	}

}
