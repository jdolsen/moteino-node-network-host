package com.digitalbotanist.mnnHost.valueHandler;

import com.digitalbotanist.mnnHost.events.NodeEvent;

public class PercentChangedValueHandler extends ValueHandler {

	double value; 
	transient Double previousVal=null; 
	
	public PercentChangedValueHandler(String id, Double value, NodeEvent event) {
		super(id, event, "Percent Changed " + value.toString(), "Percent Changed " + value.toString());
		if(value < 0){
			value = value * -1.0;
		}
		this.value = value;
	}

	@Override
	public boolean valueTriggersEvent(double val) {
		System.out.println("PercentChanged Trigger called with value: " +val + " previous value: " + previousVal + "and amount to trigger: " + value);

		if(previousVal == null){
			previousVal = val;
			return false;
		}else{
			double diff = previousVal-val;
			
			if(diff < 0){
				diff = diff * -1.0;
			}
			
			double percentDiff = (diff/previousVal)*100;			
			previousVal = val;
			
			return percentDiff >= value;			
		}
	}

}
