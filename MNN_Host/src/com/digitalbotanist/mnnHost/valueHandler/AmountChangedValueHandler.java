package com.digitalbotanist.mnnHost.valueHandler;

import com.digitalbotanist.mnnHost.events.NodeEvent;

public class AmountChangedValueHandler extends ValueHandler{

	double value;
	transient Double previousVal=null;
	
	public AmountChangedValueHandler(String id, Double value, NodeEvent event) {
		super(id, event, "Amount Changed " + value.toString(), "Amount Changed " + value.toString());		
		if(value < 0){
			value = value * -1.0;
		}
		this.value = value;
	}

	@Override
	public boolean valueTriggersEvent(double val) {
		System.out.println("AmountChanged Trigger called with value: " +val + " previous value: " + previousVal + "and amount to trigger: " + value);
		if(previousVal == null){
			previousVal = val;
			return false;
		}else{
			double diff = previousVal-val;
			previousVal = val;
			if(diff < 0){
				diff = diff * -1.0;
			}
			
			return diff >= value;			
		}
	}

}
