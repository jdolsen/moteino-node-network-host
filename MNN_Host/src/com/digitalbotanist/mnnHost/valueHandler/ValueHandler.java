package com.digitalbotanist.mnnHost.valueHandler;
import java.util.HashMap;

import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.digitalbotanist.mnnHost.events.NodeEvent;


public abstract class ValueHandler extends JPanel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String id;
	private NodeEvent event;
	String handlerType;
	boolean triggered = false;
	private JCheckBox check = new JCheckBox();
	private boolean triggerStateDependent;
	public long timeLastTriggered;
	public long numTriggers;
	
	public void setTriggerStateDependent(boolean triggerStateDependent) {
		this.triggerStateDependent = triggerStateDependent;
	}

	ValueHandler(String id, NodeEvent event, String handlerType, String label){
		this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		this.id = id;
		this.setEvent(event);
		this.handlerType = handlerType;
		
		this.add(getCheck());
		this.add(new JLabel(id + " " + label));
		triggerStateDependent = true;
		timeLastTriggered=-1;
		numTriggers=0;
	}
	
	public boolean trigger(HashMap<String, Double> dataItems){
		System.out.println("getting item: " + id);
		Double val = dataItems.get(id);
		if(val!=null){
			System.out.println("Got a non-null data value for this handler.  triggering");
			if(valueTriggersEvent(val)){//Value could trigger an event
				//TODO: update timeLastTriggered here
				timeLastTriggered=System.currentTimeMillis();
				if(!triggerStateDependent || !triggered){
					System.out.println("value triggers event (or handler is not trigger-state dependent)");
					triggered = true;
					getEvent().execute(id, handlerType, val);
					return true;
				/*}else if(!triggered){
					System.out.println("executing event since not already triggered");
					triggered = true;
					getEvent().execute(id, handlerType, val);
					return true;
				 */
				}else{
					//value would trigger event but was triggered already
					System.out.println("Event already in triggered state - not retriggering");
				}
			}else{
				System.out.println("Value does not trigger an event");
				triggered = false;
			}
		}else{
			System.out.println("Called on null value");
		}
		return false;		
	}

	abstract public boolean valueTriggersEvent(double val);

	public NodeEvent getEvent() {
		return event;
	}

	public void setEvent(NodeEvent event) {
		this.event = event;
	}

	public JCheckBox getCheck() {
		return check;
	}

	public void setCheck(JCheckBox check) {
		this.check = check;
	}	
}
