package com.digitalbotanist.mnnHost.valueHandler;

import com.digitalbotanist.mnnHost.events.NodeEvent;

public class MinValueHandler extends ValueHandler {

	double value;
	
	public MinValueHandler(String id, Double value, NodeEvent event) {
		super(id, event, "under min value", "Min " + value);
		this.value = value;

	}

	@Override
	public boolean valueTriggersEvent(double val) {
		if(val < value){
			return true;
		}
		return false;
	}
}
