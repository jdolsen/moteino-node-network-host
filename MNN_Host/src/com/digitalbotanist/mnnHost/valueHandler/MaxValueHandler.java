package com.digitalbotanist.mnnHost.valueHandler;
import java.util.HashMap;

import com.digitalbotanist.mnnHost.events.NodeEvent;

public class MaxValueHandler extends ValueHandler {

	Double value;
		
	public MaxValueHandler(String id, Double maxValue, NodeEvent event) {		
		super(id, event, "over max value", "Max " + maxValue);
		System.out.println("Value is: " + maxValue);
		this.value = maxValue;
	}

	@Override
	public boolean valueTriggersEvent(double val) {
		if(val > value){
			return true;
		}
		return false;
	}

}
