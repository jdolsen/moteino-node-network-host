package com.digitalbotanist.mnnHost.valueHandler;

import com.digitalbotanist.mnnHost.events.NodeEvent;

public class AnyValueHandler extends ValueHandler {
	
	public AnyValueHandler(String id, NodeEvent event) {
		super(id, event, "AnyValue", "Any Value");
		super.setTriggerStateDependent(false);
	}

	@Override
	public boolean valueTriggersEvent(double val) {		
		return true;
	}

}
