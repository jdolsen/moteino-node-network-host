package com.digitalbotanist.mnnHost.valueHandler;

import com.digitalbotanist.mnnHost.events.NodeEvent;

public class EqualValueHandler extends ValueHandler {

	double value;
	
	public EqualValueHandler(String id, Double value, NodeEvent event) {
		super(id, event, "equal", "Equals " + value);
		this.value = value;
	}

	@Override
	public boolean valueTriggersEvent(double val) {
		if(val==value){
			return true;
		}
		return false;
	}

}
