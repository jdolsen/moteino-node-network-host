package com.digitalbotanist.mnnHost.valueHandler;

import com.digitalbotanist.mnnHost.events.NodeEvent;

public class ChangedValueHandler extends ValueHandler {

	private Double previousVal = null;

	public ChangedValueHandler(String id, NodeEvent event) {
		super(id, event, "Changed Value", "Changed Value");
	}

	@Override
	public boolean valueTriggersEvent(double val) {
		System.out.println("Changed Trigger called with value: " +val + 
				" and previous value: " + previousVal);
		if(previousVal == null){
			previousVal = val;
			return false;
		}else{			
			boolean retVal = (previousVal!=val);
			previousVal = val;
			return retVal;
		}
	}

}
