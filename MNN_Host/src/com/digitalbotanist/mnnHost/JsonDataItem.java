package com.digitalbotanist.mnnHost;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import com.digitalbotanist.mnnHost.serial.ParsedObject;
import com.google.gson.Gson;

public class JsonDataItem {
	public String version ="1.0.0";
	public ArrayList<JsonDataEntry> datastreams = new ArrayList<JsonDataEntry>();
	//TODO: implement below
	//public ArrayList<JsonDataPoint> datapoints = new ArrayList<JsonDataPoint>();
	public String getJsonString(){
		Gson gson = new Gson();
		
		return gson.toJson(this);
	}
	
	public JsonDataItem(ParsedObject o, Command c){
		
		for(CommandField cf: c.fields.values()){
			datastreams.add(new JsonDataEntry(cf.fieldname, o.getFieldValue(cf.fieldname).getValue()));
		}
		
		//datastreams.add(new JsonDataEntry(channelId,value));		
	}
}
