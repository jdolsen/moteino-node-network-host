package com.digitalbotanist.mnnHost;
import java.awt.Color;
import java.util.Random;

import javax.swing.JCheckBox;
import javax.swing.JLabel;


public class StatusDisplayObject {
	public JLabel label;
	public JLabel data;
	public JCheckBox box;
	String fName = null;

	
	StatusDisplayObject(JLabel label, JLabel data, JCheckBox box){
		this.label = label;
		this.data = data;
		this.box = box;
	}
	

}
