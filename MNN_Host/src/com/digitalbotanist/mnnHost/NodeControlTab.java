package com.digitalbotanist.mnnHost;
import java.awt.Button;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;


public class NodeControlTab extends JScrollPane {

	JPanel panel = null;
	
	public NodeControlTab(HashMap<Integer, Node> nodes) {
		initializeTab(nodes);
	}

	public void initializeTab(HashMap<Integer, Node> nodes) {	
		panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		this.setViewportView(panel);
		
		//add a series of panels for each node		
		System.out.println("Adding nodes to panel: " + nodes.size());
		for(Node n:nodes.values()){
			panel.add(n);
		}
		
		this.getVerticalScrollBar().setUnitIncrement(20);
		this.revalidate();
		this.repaint();
		
	}
}
