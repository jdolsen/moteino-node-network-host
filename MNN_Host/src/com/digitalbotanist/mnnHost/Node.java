package com.digitalbotanist.mnnHost;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class Node extends JPanel{

	private static final long serialVersionUID = 1L;
	private int nodeId;
	private String nodeName;
	private boolean hasName = false;
	private String status = "Unconnected";
	private int connectionStatus = 10;
	private double numFailedMessages = 0;
	private double numSuccessfulMessages = 0;
	
	//commands available to be exchanged with this node
	//receivable and sendable are with respect to the node
	private HashMap<String, Command> receivableCommands = new HashMap<String, Command>();
	private HashMap<String, Command> sendableCommands = new HashMap<String, Command>();
	private JLabel nodeNameLabel;
	private JLabel nodeStatusLabel;
	private JLabel messageStatusLabel;
	private JPanel nodeStatusPanel;
	private JPanel receivableCommandsPanel;
	private JPanel sendableCommandsPanel;
	
	ActionListener a = new ActionListener(){

		@Override
		public void actionPerformed(ActionEvent arg0) {			
			printNode();		
		}
		
	};
	
	public Node(int nodeId){

		this.nodeId = nodeId;
		this.nodeName = Integer.toString(nodeId);//"None";
		this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		
		nodeStatusPanel = new JPanel();
		nodeStatusPanel.setLayout(new BoxLayout(nodeStatusPanel, BoxLayout.Y_AXIS));
		
		if(nodeId%2==0){
			nodeStatusPanel.setBackground(Color.GREEN);
		}else{
			nodeStatusPanel.setBackground(Color.GREEN.darker());
		}

		nodeStatusPanel.add(new JSeparator());
		nodeNameLabel = new JLabel();		
		//nodeNameLabel.setToolTipText("bbbbbb");
		nodeNameLabel.setText("Node " + nodeId);
		nodeNameLabel.setFont(new Font("Tahoma", Font.PLAIN, 18));
		nodeNameLabel.setAlignmentY(Component.TOP_ALIGNMENT);
		nodeNameLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
		nodeNameLabel.setHorizontalAlignment(SwingConstants.LEFT);
		nodeStatusPanel.add(nodeNameLabel);
		
		//JPanel statusSubPanel = new JPanel();
		//statusSubPanel.setLayout(new BoxLayout(statusSubPanel, BoxLayout.X_AXIS));		
		
		//Box statusBox = Box.createHorizontalBox();
		
		nodeStatusLabel = new JLabel();
		nodeStatusLabel.setText("Status: " + status + ": " + connectionStatus);
		nodeStatusLabel.setFont(new Font("Tahoma", Font.PLAIN, 18));
		nodeStatusLabel.setAlignmentY(Component.TOP_ALIGNMENT);
		nodeStatusLabel.setAlignmentX(Component.LEFT_ALIGNMENT);

		//statusBox.add(nodeStatusLabel);
		//statusBox.add(Box.createHorizontalGlue());
		messageStatusLabel = new JLabel();
		messageStatusLabel.setText("Message Status: None");
		messageStatusLabel.setFont(new Font("Tahoma", Font.PLAIN, 18));
		messageStatusLabel.setAlignmentY(Component.TOP_ALIGNMENT);
		messageStatusLabel.setAlignmentX(Component.LEFT_ALIGNMENT);

		//statusBox.add(messageStatusLabel);
		
		nodeStatusPanel.add(nodeStatusLabel);
		nodeStatusPanel.add(messageStatusLabel);
		//nodeStatusPanel.add(statusBox);
		this.add(nodeStatusPanel);
		
		receivableCommandsPanel = new JPanel();
		receivableCommandsPanel.setLayout(new BoxLayout(receivableCommandsPanel, BoxLayout.PAGE_AXIS));
		receivableCommandsPanel.add(new JSeparator());
		sendableCommandsPanel = new JPanel();
		sendableCommandsPanel.setLayout(new BoxLayout(sendableCommandsPanel, BoxLayout.PAGE_AXIS));
		sendableCommandsPanel.add(new JSeparator());
		
		this.add(receivableCommandsPanel);
		this.add(sendableCommandsPanel);		
		updateDisplay();	
	}
	
	public int getNodeId() {
		return nodeId;
	}

	public void setNodeId(int nodeId) {
		this.nodeId = nodeId;
	}

	public String getNodeName() {
		return nodeName;
	}

	public void setNodeName(String nodeName) {
		this.nodeName = nodeName;
		hasName = true;
		nodeNameLabel.setText(nodeName + " on channel: " + nodeId);
		this.revalidate(); 
		this.repaint();
	}

	public boolean addReceivableCommand(String command){

		if(receivableCommands.containsKey(command)){
			return false;
		}
		Command c = new Command(command, nodeId, true);
		receivableCommands.put(command, c);
		
		System.out.println("Adding command: " + command);
		c.setAlignmentX(Component.LEFT_ALIGNMENT);
		receivableCommandsPanel.add(c);		
		this.revalidate();///validate() and 
		this.repaint();
		return true;
		//CommandEnumerator.getInstance().add(command, nodeId);
	}
	
	public boolean addSendableCommand(String command){
		if(sendableCommands.containsKey(command)){
			return false;
		}
		Command c = new Command(command, nodeId, false);
		sendableCommands.put(command, c);
		//sendableCommands.put(command, new Command(command, nodeId));
		System.out.println("Adding command: " + command);
		c.setAlignmentX(Component.LEFT_ALIGNMENT);
		sendableCommandsPanel.add(c);		
		this.revalidate();///validate() and 
		this.repaint();
		return true;
	}
	
	public Command getReceivableCommand(String command){
		return receivableCommands.get(command);
	}
	
	public Command getSendableCommand(String command){
		return sendableCommands.get(command);
	}
	
	public String toString(){
		return nodeName;
	}

	public void printNode() {
		System.out.println("Node " + nodeName + " on channel: " + nodeId + " accepts these commands: ");
		for(Command c: receivableCommands.values()){
			c.print();
			//stem.out.println("  " + c.commandString);			
		}
		
		System.out.println("Node " + nodeName + " on channel: " + nodeId + " sends these messages: ");
		for(Command c: sendableCommands.values()){
			c.print();	
		}
		
	}
	
	private void updateDisplay(){
		for(Command c: receivableCommands.values()){
			this.add(c);
		}
	}
	
	public void setStatus(int status){
		System.out.println("Setting status for node " + this.nodeId + " to : " + status);
		connectionStatus = status;		
		if(status < 3){
			this.status = "Connected"; 
			//nodeStatusLabel.setText("Connected");
			nodeStatusPanel.setBackground(Color.GREEN);
			for(Command c: receivableCommands.values()){
				c.enableCommand();
			}
			for(Command c: sendableCommands.values()){
				c.enableCommand();
			}
		}else if(status >=3 && status <10){	
			this.status = "Disconnected";
			//nodeStatusLabel.setText("Disconnected");
			nodeStatusPanel.setBackground(Color.RED);
			for(Command c: receivableCommands.values()){
				c.disableCommand();
			}
			for(Command c: sendableCommands.values()){
				c.disableCommand();
			}
		}else if(status >= 10){
			this.status = "Unconnected";
			//nodeStatusLabel.setText("Unconnected");
			nodeStatusPanel.setBackground(Color.GRAY);
			for(Command c: receivableCommands.values()){
				c.disableCommand();
			}
			for(Command c: sendableCommands.values()){
				c.disableCommand();
			}
		}
		nodeStatusLabel.setText("Status: " + this.status + ": " + connectionStatus);
		this.revalidate();
		this.repaint();
	}

	public void setMessageFailed() {
		// TODO A message sent to this node failed make a note of it in the status
		//degrade connection status
		nodeStatusPanel.setBackground(Color.yellow);
	}
	
	public void setMessageSuccess(){
		//upgrade connection status
		nodeStatusPanel.setBackground(Color.green);
	}

	public boolean hasName() {
		return hasName;
	}

	public void clearCommands() {
		
		receivableCommands.clear();
		sendableCommands.clear();
		
		receivableCommandsPanel.removeAll();
		sendableCommandsPanel.removeAll();
	}
	
	public void resetNode(){
		this.nodeName = "Node " + Integer.toString(nodeId);
		nodeNameLabel.setText(nodeName + " on channel: " + nodeId);
		this.hasName = false;
		clearCommands();
		setStatus(10);
	}
}
