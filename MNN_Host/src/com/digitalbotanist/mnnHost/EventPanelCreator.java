package com.digitalbotanist.mnnHost;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Calendar;
import java.util.Date;

import javax.swing.text.AttributeSet;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.border.TitledBorder;
import javax.swing.text.BadLocationException;
import javax.swing.text.Element;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;


public class EventPanelCreator {

	JTextPane textPane = new JTextPane();
	StyledDocument doc = null;
	//JTextArea textArea = new JTextArea();
	JScrollPane scrollPane = new JScrollPane();
	//JFrame parent = null;
	
	//TODO: create a log file
	File log = null;
	//if the number of events reaches maxEvents, dump the most recent half into the log file
	int MAX_EVENTS = 6000;
	int events = 0;
	
	public EventPanelCreator(Container container){
		//this.parent = parent;
		//event panel
		doc = textPane.getStyledDocument();
		JPanel EventPanel = new JPanel();
		EventPanel.setBorder(new TitledBorder(null, "Event Log", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagConstraints gbc_EventPanel = new GridBagConstraints();
		gbc_EventPanel.insets = new Insets(0, 0, 0, 5);
		gbc_EventPanel.fill = GridBagConstraints.BOTH;
		gbc_EventPanel.gridx = 0;
		gbc_EventPanel.gridy = 1;
		container.add(EventPanel, gbc_EventPanel);
		EventPanel.setLayout(new BorderLayout(0, 0));
				
		EventPanel.add(scrollPane);
		textPane.setEditable(false);
		scrollPane.setViewportView(textPane);
		
		
		log = new File("MNNlog-" + 
				Calendar.getInstance().get(Calendar.MONTH) + "-" +
				Calendar.getInstance().get(Calendar.DAY_OF_MONTH) + "-" +
				Calendar.getInstance().get(Calendar.YEAR) + "--" +
				Calendar.getInstance().get(Calendar.HOUR_OF_DAY) + "-" +
				Calendar.getInstance().get(Calendar.MINUTE)
				+".txt");
		

	}
	
	public void addEvent(String event){
		try {
			Date d = Calendar.getInstance().getTime();
			doc.insertString(doc.getLength(), d.toString() + "| " + event + "\n", null);
			textPane.setCaretPosition(doc.getLength());
			dumpExcessEvents();
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
	}
	
	public void addEvent(String event, Color c){
		try {
			Date d = Calendar.getInstance().getTime();
			StyleContext sc = StyleContext.getDefaultStyleContext(); 
			AttributeSet aset = sc.addAttribute(SimpleAttributeSet.EMPTY, StyleConstants.Foreground, c);
			doc.insertString(doc.getLength(), d.toString() + "| " + event + "\n", aset);
			textPane.setCaretPosition(doc.getLength());
			dumpExcessEvents();
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
	}

	private void dumpExcessEvents() {
		events++;
		if(events>=MAX_EVENTS){
			//remove half the events and change update the events counter
			try {			
				if (!log.exists()) {
					log.createNewFile();
				}
				FileWriter fw = new FileWriter(log.getAbsoluteFile(),true);
				BufferedWriter bw = new BufferedWriter(fw);
				//System.out.println("writing buffer to: " + log.getAbsolutePath());
				
				for(int i = 0; i<MAX_EVENTS/2; ++i){
					//get the first line in the document
					Element root = doc.getDefaultRootElement();
					Element first = root.getElement(0);
					String line = doc.getText(first.getStartOffset(), first.getEndOffset()-first.getStartOffset());
					
					//store the text and remove it										
					bw.write(line);
					bw.newLine();
					doc.remove(first.getStartOffset(), first.getEndOffset());
					events--;
				}			
				bw.flush();
				bw.close();
				
				

			} catch (BadLocationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}		
	}
	
	
}
