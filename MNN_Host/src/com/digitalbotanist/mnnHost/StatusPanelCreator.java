package com.digitalbotanist.mnnHost;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import javax.swing.AbstractButton;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import com.digitalbotanist.javaPlots.BasePlot;
import com.digitalbotanist.javaPlots.MultiPlot;
import com.digitalbotanist.javaPlots.TimePlot;


public class StatusPanelCreator {
	int yCurrent = 0;
	//JPanel statusPanel = new JPanel();
	
	JPanel statusSubPanel = new JPanel();
	MultiPlot graphs = null;
	Container parent = null;
	
	DefaultComboBoxModel model = new DefaultComboBoxModel();
	JComboBox nodeComboBox = new JComboBox(model);						       //nodeid    //command Id   //field
	//HashMap<Integer, HashMap<String, HashMap<String, StatusDisplayObject>>> objects= new HashMap<Integer, HashMap<String, HashMap<String, StatusDisplayObject>>>();
	
	HashMap<String, StatusDisplayObject> objects = new HashMap<String, StatusDisplayObject>();
	
	public void addNodeSelection(int nodeNum){
		if(model.getIndexOf(Integer.toString(nodeNum))==-1){
			model.addElement(Integer.toString(nodeNum));
		}
		//nodeComboBox.addItem(Integer.toString(nodeNum));		
	}
	
	//TODO: only display the items selected in the drop-down list
	
	public StatusPanelCreator(Container container, MultiPlot graphs){
		this.graphs = graphs;
		this.parent = container;

		JPanel statusPanel = new JPanel();		
		statusPanel.setBorder(new TitledBorder(null, "Status", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagConstraints gbc_statusPanel = new GridBagConstraints();
		gbc_statusPanel.insets = new Insets(0, 0, 5, 0);
		gbc_statusPanel.fill = GridBagConstraints.BOTH;
		gbc_statusPanel.gridx = 1;
		gbc_statusPanel.gridy = 0;
		
		parent.add(statusPanel, gbc_statusPanel);
		statusPanel.setLayout(new BorderLayout(0,0));	
		
		JScrollPane scrollPane_1 = new JScrollPane();
		statusPanel.add(scrollPane_1, BorderLayout.CENTER);
		
		scrollPane_1.setViewportView(statusSubPanel);
		
		GridBagLayout gbl_statusPanel = new GridBagLayout();
		gbl_statusPanel.columnWidths = new int[]{0, 0, 0, 0, 0};
		//TODO:  row heights and weights need to be updated if adding more than 15 elements
		gbl_statusPanel.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_statusPanel.columnWeights = new double[]{0.0, 1.0, 0.0, Double.MIN_VALUE};
		gbl_statusPanel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		statusSubPanel.setLayout(gbl_statusPanel);
		
		//add the node selector label and combobox
		JLabel lblNode = new JLabel("Node");
		GridBagConstraints gbc_lblNode = new GridBagConstraints();
		gbc_lblNode.anchor = GridBagConstraints.WEST;
		gbc_lblNode.insets = new Insets(0, 0, 5, 5);
		gbc_lblNode.gridx = 0;
		gbc_lblNode.gridy = 0;
		statusSubPanel.add(lblNode, gbc_lblNode);
		
		GridBagConstraints gbc_comboBox_1 = new GridBagConstraints();
		gbc_comboBox_1.gridwidth = 3;
		gbc_comboBox_1.insets = new Insets(0, 0, 5, 5);
		gbc_comboBox_1.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBox_1.gridx = 1;
		gbc_comboBox_1.gridy = 0;
		statusSubPanel.add(nodeComboBox, gbc_comboBox_1);
		yCurrent=1;
		
		nodeComboBox.addItem("All");
	}
	
	public void createStatusPanelElement(final String plotName){//, final String plotName, 
			//int id){//, String command, String field){
				
		//addNodeSelection(id);
		
		//Status panel element 
		JLabel lblNewLabel = new JLabel(plotName);
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel.gridx = 0;
		gbc_lblNewLabel.gridy = yCurrent;
		statusSubPanel.add(lblNewLabel, gbc_lblNewLabel);
		
		JLabel value = new JLabel("");
		GridBagConstraints gbc_humidityValue = new GridBagConstraints();
		gbc_humidityValue.insets = new Insets(0, 0, 5, 5);
		gbc_humidityValue.gridx = 1;
		gbc_humidityValue.gridy = yCurrent;
		statusSubPanel.add(value, gbc_humidityValue);
		
		JCheckBox checkBox = new JCheckBox("");
		checkBox.setSelected(true);
		GridBagConstraints gbc_checkBox = new GridBagConstraints();
		gbc_checkBox.insets = new Insets(0, 0, 5, 0);
		gbc_checkBox.gridx = 2;
		gbc_checkBox.gridy = yCurrent;
		statusSubPanel.add(checkBox, gbc_checkBox);
		
		Color c = randomColor();
		JButton button = new JButton("");
		button.setBackground(c);
		graphs.setPlotColor(plotName, c);
		
		GridBagConstraints gbc_btnNewButton_1 = new GridBagConstraints();
		gbc_btnNewButton_1.fill = GridBagConstraints.BOTH;
		gbc_btnNewButton_1.insets = new Insets(0, 0, 5, 0);
		gbc_btnNewButton_1.gridx = 3;
		gbc_btnNewButton_1.gridy = yCurrent;
		statusSubPanel.add(button, gbc_btnNewButton_1);
		
		checkBox.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent event) {
				AbstractButton abstractButton = (AbstractButton) event.getSource();
			    boolean selected = abstractButton.getModel().isSelected();
			    graphs.setPlotVisibiltiy(plotName, selected);
			}			
		});
		
		button.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent event) {
				AbstractButton abstractButton = (AbstractButton) event.getSource();
				
				Color newColor = JColorChooser.showDialog(
	                     parent,
	                     "Choose Plot Color for " + plotName,
	                     abstractButton.getBackground());//will eventually be the button's color
				if(newColor!=null){
					graphs.setPlotColor(plotName, newColor);
					abstractButton.setBackground(newColor);
				}
			}
			
		});
		
		
		yCurrent++;
		statusSubPanel.revalidate();
		statusSubPanel.repaint();
		StatusDisplayObject statObj = new StatusDisplayObject(lblNewLabel, value, checkBox);
		addField(plotName, statObj);
	}
	
	Color randomColor(){
		return new Color(new Random().nextInt(255), new Random().nextInt(255), new Random().nextInt(255));
	}

	
	public void updateFieldStatusValue(String nodeName, Double val) {
		StatusDisplayObject obj = objects.get(nodeName);
		if(obj!=null){
			obj.data.setText(val.toString());
		}
	}
	
	public StatusDisplayObject getStatusDisplayObject(String nodeName){
		return objects.get(nodeName);
	}
	
	private void addField(String name, StatusDisplayObject statObj) {
		objects.put(name, statObj);		
	}
}
