package com.digitalbotanist.mnnHost;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.digitalbotanist.mnnHost.serial.NodeMessage;
import com.digitalbotanist.mnnHost.serial.SerialConnector;


public class Command extends JPanel{

	private static final long serialVersionUID = 1L;
	public String commandString;
	JButton sendButton;// = new JButton("Send");
	public LinkedHashMap<String, CommandField> fields = new LinkedHashMap<String, CommandField>();
	int nodeId; //TODO: have a lookup of node name to node-ids.  Use this exclusively to find the id to send to
	boolean isReceivable;
	
	public Command(String name, int nodeId, boolean isReceivable){
		this.isReceivable = isReceivable;
		this.nodeId = nodeId;
		System.out.println("New Command Created: " + name);
		commandString = name;
		this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		this.add(new JLabel(name+"  "));
		if(isReceivable){
			sendButton = new JButton("Send");		
		}else{
			sendButton = new JButton("Request Update");			
		}
		
		sendButton.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				System.out.println("Sending: " + getCommandMessageString());
				NodeMessage mssg = new NodeMessage(getCommandMessageString(), nodeId);
				SerialConnector.getInstance().sendSerial(mssg);
			}			
		});
		this.add(sendButton);
		this.setBackground(Color.green.brighter());
	}
	
	public void addField(String field){				
		if(!fields.containsKey(field)){
			CommandField f = new CommandField(field, isReceivable);
			fields.put(field, f);
			//update the display	
			int index = this.getIndexOfButton();
			this.add(f.box, index);
			this.add(new JLabel(" " + field+" "), index);
			
			this.revalidate();///validate() and 
			this.repaint();
			System.out.println("added field: " + field);
		}		
	}
	
	private int getIndexOfButton(){
		for(int i=0; i<this.getComponentCount(); ++i){
			if(getComponent(i)==sendButton){
				return i;
			}
		}
		return 0;
		
	}

	public void print() {
		System.out.println("  " + getCommandString());
		for(String field: fields.keySet()){
			System.out.println("    " + field);
		}	
	}
	
	public String getCommandMessageString(){
		//for each command field, get the value from the associated text box
		StringBuilder b = new StringBuilder();
		b.append(getCommandString() + "|");
		if(isReceivable){//only receivable commands have fields to be sent
			for(CommandField f:fields.values()){
				b.append(f.fieldname);
				b.append(":");
				b.append(f.box.getText());
				b.append(";");
			}
		}
		return b.toString();
	}

	public void enableCommand() {
		sendButton.setEnabled(true);
		for(CommandField f: fields.values()){
			f.box.setEnabled(true);
			f.box.setEditable(true);
		}
		this.setBackground(Color.green.brighter().brighter().brighter());
	}

	public void disableCommand() {
		sendButton.setEnabled(false);	
		for(CommandField f: fields.values()){
			f.box.setEnabled(false);
			f.box.setEditable(false);
		}
		this.setBackground(Color.gray);
	}

	public String getCommandString() {
		return commandString;
	}
}
