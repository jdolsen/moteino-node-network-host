package com.digitalbotanist.mnnHost;
import javax.swing.JTextField;


public class CommandField {

	public String fieldname;
	JTextField box;
	
	public CommandField(String field, boolean isReceivable) {
		fieldname=field;
		box = new JTextField();
		if(!isReceivable){
			box.setEditable(false);
		}
	}

}
