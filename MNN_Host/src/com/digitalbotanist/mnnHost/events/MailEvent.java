package com.digitalbotanist.mnnHost.events;
import java.util.Calendar;
import java.util.Properties;

import javax.mail.MessagingException;


public class MailEvent extends NodeEvent {

	String destAdx;
	String userAdx;
	String pw;	
	Properties props = new Properties();
	
	
	public MailEvent(String destAdx, String userAdx, String pw) {
		super("No Message");
		this.destAdx = destAdx;
		this.userAdx = userAdx;
		this.pw = pw;
		/*props.put("mail.smtp.auth", "true");
		
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");
		
		props.put("mail.pop3s.starttls.enable", "true");
		props.put("mail.pop3s.host", "pop.gmail.com");
		props.put("mail.pop3s.port", "995");
		*/
	}

	public void addProperty(String prop, String value){
		props.put(prop, value);
	}
	
	@Override
	public void doEvent(String id, String eventType, String message, Double val) {
		System.out.println("Mailing event");
		Mailer m = new Mailer();
		//mailing takes a long time.  Create a new thread to do this
		Thread t = new Thread(new Runnable(){
			@Override
			public void run() {
				Calendar c = Calendar.getInstance();
				try {
					m.sendMail(null, id + " value " + val + " is " + eventType, destAdx, userAdx, pw, props);
				} catch (MessagingException e) {
					e.printStackTrace();
				}
				System.out.println("Event mailed at " + c.getTime().toString());
			}		
		});
		t.start();		
	}

}
