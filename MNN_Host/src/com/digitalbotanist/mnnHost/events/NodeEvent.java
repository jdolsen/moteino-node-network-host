package com.digitalbotanist.mnnHost.events;
import java.io.Serializable;


public abstract class NodeEvent implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String message;
	
	NodeEvent(String message){
		this.message = message;
	}
	
	public void execute(String id, String eventType, Double val) {
		System.out.println(id + " has caused an " + eventType + " event to trigger: " + message);
		doEvent(id, eventType, message, val);
	}
	
	public abstract void doEvent(String id, String eventType, String message, Double val);


}
