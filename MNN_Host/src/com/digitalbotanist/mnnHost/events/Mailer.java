package com.digitalbotanist.mnnHost.events;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.sun.mail.imap.IMAPStore;
import com.sun.mail.pop3.POP3Store;

import org.apache.commons.io.IOUtils;
import org.apache.commons.io.IOUtils.*;


public class Mailer {
	/*
	Properties props = new Properties();
	{
		props.put("mail.smtp.auth", true);
		
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");
		
		props.put("mail.pop3s.starttls.enable", "true");
		props.put("mail.pop3s.host", "pop.gmail.com");
		props.put("mail.pop3s.port", "995");
				
		//props.put("mail.imaps.host", "imap.gmail.com");
		//props.put("mail.imaps.port", "993");
		//props.put("mail.imaps.starttls.enable", "true");		
	}*/

	public void sendMail(String subject, String body, String to, String username, 
			String password, Properties props) throws AddressException, MessagingException{
		Session session = Session.getInstance(props,
		  new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		  });
 
 
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(username));
			message.setRecipients(Message.RecipientType.TO,
				InternetAddress.parse(to));
			message.setSubject(subject);
			message.setText(body);
 
			Transport.send(message);			
 
			System.out.println("Done");
 
	}
	
	public void getMail(String username, String password, Properties props){
		System.out.println("getting mail...");
		Session emailSession = Session.getInstance(props);
		try {
			System.out.println("getting pop store...");
			POP3Store emailStore = (POP3Store) emailSession.getStore("pop3s");
			//IMAPStore emailStore = (IMAPStore) emailSession.getStore("imaps");

			System.out.println("connecting...");
			emailStore.connect(username, password);
			Folder emailFolder = emailStore.getFolder("INBOX");
			emailFolder.open(Folder.READ_WRITE);
			
			//emailFolder.
			System.out.println("getting messages... " + emailFolder.getMessageCount());
			//int count = 
			Message[] messages = emailFolder.getMessages();
			 //messages = emailFolder.getMessages(0, 4);
			System.out.println("received messages: " + messages.length);
			for (int i = 0; i < messages.length; i++) {
				System.out.println("getting message " +i);
				Message message = messages[i];
				System.out.println("==============================");
				System.out.println("Email #" + (i + 1));
				System.out.println("Subject: " + message.getSubject());
				System.out.println("From: " + message.getFrom()[0]);
				System.out.println("Text: " + message.getContent().toString());
				
				System.out.println("Type: " + message.getContentType());
				System.out.println("=========================================================================");
				InputStream is = message.getInputStream();
				OutputStream os = System.out;
				IOUtils.copy(is, os);
				
				System.out.println("=========================================================================");
				
				//DataHandler handler = message.getDataHandler();
				System.out.println("Type: " + message.getContentType());
			}

			emailFolder.close(false);
			emailStore.close();
		} catch (NoSuchProviderException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args){
		Mailer m = new Mailer();
	}
}
