package com.digitalbotanist.mnnHost;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import javax.net.ssl.HttpsURLConnection;

public class XivelyPostAction {
	
	private String FEED_ID;	
	private String API_KEY;
	
	XivelyPostAction(String feed, String apiKey){
		FEED_ID = feed;
		API_KEY = apiKey;		
	}
	
	void post(String data) {
		System.out.println("Writing to Xively: " + data);
		try {
			URL url = new URL("https://api.xively.com/v2/feeds/" + FEED_ID);
			HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
			
			conn.setRequestMethod("PUT");
			conn.setRequestProperty("X-ApiKey", API_KEY);
			conn.setRequestProperty("Content-Type", "application/json");
			
			conn.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
			wr.writeBytes(data);
			wr.flush();
			wr.close();
			int responseCode = conn.getResponseCode();
			
			System.out.println(responseCode);

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
