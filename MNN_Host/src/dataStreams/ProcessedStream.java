package dataStreams;

import java.awt.Color;
import java.util.Calendar;

import com.digitalbotanist.javaPlots.FinePoint;
import com.digitalbotanist.javaPlots.MultiPlot;
import com.digitalbotanist.javaPlots.TimePlot;
import com.digitalbotanist.mnnHost.CommandField;
import com.digitalbotanist.mnnHost.StatusDisplayObject;

public abstract class ProcessedStream extends TimePlot{

	public String name;
	protected MultiPlot graphs;
	StatusDisplayObject valueDisplay;
	
	public String getName(){
		return this.name;
	}
	
	public ProcessedStream(int displaySeconds, String name, MultiPlot graphs) {
		super(displaySeconds);
		this.graphs = graphs;
		this.name=name;
		// TODO Auto-generated constructor stub
	}

	public abstract Double addStreamValue(double val);
	
	public void addPoint(double val){
		graphs.addPoint(new FinePoint(Calendar.getInstance().getTimeInMillis(),val), this.name);
		valueDisplay.data.setText(Double.toString(val));
	}

	public void setCommandField(StatusDisplayObject statusDisplayObject) {
		this.valueDisplay = statusDisplayObject;
	}

}
