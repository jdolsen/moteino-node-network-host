package dataStreams;

import com.digitalbotanist.javaPlots.MultiPlot;

public class AmountChangedProcessedStream extends ProcessedStream {

	private double maxAmountChange;
	private Double previousValue = null;

	public AmountChangedProcessedStream(int displaySeconds, String name, double maxAmountChange, MultiPlot graphs) {
		super(displaySeconds, name, graphs);
		this.maxAmountChange = maxAmountChange;
	}

	@Override
	public Double addStreamValue(double val) {
		if(previousValue==null){
			this.addPoint(val);
			previousValue = new Double(val);
			return val;
		}else{
			double diff = Math.abs(previousValue-val);			
			
			if(diff>maxAmountChange){
				return null;
			}else{
				previousValue=val;
				this.addPoint(val);
				return val;
			}			
		}		
	}

}
