package dataStreams;

import java.awt.Color;
import java.util.ArrayList;

import com.digitalbotanist.javaPlots.FinePoint;
import com.digitalbotanist.javaPlots.MultiPlot;

public class WindowAveragedProcessedStream extends ProcessedStream {

	ArrayList<Double> previousValues = new ArrayList<Double>();
	int windowSize = 5;
	
	public WindowAveragedProcessedStream(int displaySeconds, String name, MultiPlot graphs) {
		super(displaySeconds, name, graphs);
	}

	@Override
	public Double addStreamValue(double val) {
		//update values to average
		previousValues.add(val);
		if(previousValues.size()>windowSize){
			//remove the oldest value
			previousValues.remove(0);
		}
		
		//average the values and add the new value to the stream
		if(previousValues.size()>=windowSize){
			Double sum=0.;
			for(Double v : previousValues){
				sum+=v;
			}
			double avg = sum/windowSize;
			this.addPoint(avg);
			return avg;
			
		}
		return null;
	}

}
