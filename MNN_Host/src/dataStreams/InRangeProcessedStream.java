package dataStreams;

import com.digitalbotanist.javaPlots.MultiPlot;

public class InRangeProcessedStream extends ProcessedStream {

	public InRangeProcessedStream(int displaySeconds, String name, double upper, double lower, MultiPlot graphs) {
		super(displaySeconds, name, graphs);
		if(upper>lower){
			this.upper=upper;
			this.lower=lower;
		}else{
			this.upper=lower;
			this.lower=upper;
		}
	}
	
	double upper;
	double lower;
	
	@Override
	public Double addStreamValue(double val) {
		System.out.println("Updating inRange processor: " + name + " with value: " + val);
		if(val<=upper && val>=lower){
			this.addPoint(val);
			return val;
		}
		return null;
	}

}
