package dataStreams;

import java.awt.Color;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import org.apache.commons.math.MathException;
import org.apache.commons.math.distribution.NormalDistributionImpl;

import com.digitalbotanist.javaPlots.FinePoint;
import com.digitalbotanist.javaPlots.MultiPlot;
import com.digitalbotanist.javaPlots.ScatterPlot;
import com.digitalbotanist.javaPlots.TimePlot;

public class OutlierProcessedStream extends ProcessedStream {

	ArrayList<FinePoint> previousValues = new ArrayList<FinePoint>();
	int windowMinSize = 20;
	String rPlotName;
	TimePlot rPlot;
	
	double prevCVar = 0;
	double prevR = 0;
	
	public OutlierProcessedStream(int displaySeconds, String name, MultiPlot graphs) {
		super(displaySeconds, name, graphs);
		this.rPlotName = name+"regression";	
		rPlot = new TimePlot(displaySeconds);
		rPlot.setYAutoScale(false);
		graphs.addTimePlot(rPlot, rPlotName);
		graphs.setPlotColor(rPlotName, Color.black);
	}

	@Override
	public Double addStreamValue(double val) {
		System.out.println("Updating outlier processor: " + name + " with value: " + val);
		//look at the last window number of points and determine if the current point is an outlier
		
		//regardless, add the point to the end of the previous values
		
		previousValues.add(new FinePoint(Calendar.getInstance().getTimeInMillis(),val));
		if(previousValues.size()>windowMinSize){
			//remove the oldest value
			previousValues.remove(0);
		}
		
		//get the mean of x and y
		double yBar = 0;
		double xBar = 0;
		for(FinePoint d: previousValues){
			yBar+=d.getY();
			xBar+=d.getX();
		}
		yBar=yBar/previousValues.size();
		xBar=xBar/previousValues.size();
		
		//get the difference sums of x, y, and xy
		double yDifSum = 0;
		double xDifSum = 0;
		double xyDifSum = 0;
		for(FinePoint d: previousValues){
			double yDif = d.getY() - yBar;
			yDifSum += (yDif*yDif);
			double xDif = d.getX() - xBar;
			xDifSum += (xDif*xDif);
			xyDifSum += xDif*yDif;
		}
		
		
		//calculate the std dev and correlation factor of x and y using the difference sums
		double ySigma = Math.sqrt(yDifSum/(previousValues.size()));
		double xSigma = Math.sqrt(xDifSum/(previousValues.size()));
		double yCVar = ySigma/yBar; //coefficient of variation
		double r = (xyDifSum/(ySigma*xSigma))/previousValues.size();
		//System.out.println("xB:" + xBar + " yB:" + yBar + " xx:" + xDifSum + " yy:" + yDifSum + " xy:" + xyDifSum);
		
		String rstr = new DecimalFormat("#.0###", DecimalFormatSymbols.getInstance( Locale.ENGLISH )).format(r);
		String yCVStr = new DecimalFormat("#.0###", DecimalFormatSymbols.getInstance( Locale.ENGLISH )).format(yCVar);
		System.out.println(//"xS:" + xSigma + " yS:" + ySigma + 
				" ySN:" + yCVStr +" r:" + rstr);
		
		//calculate points for the regression line
		double slope = r*(ySigma/xSigma);
		double intercept = yBar-(slope*xBar);
		double firstTime = previousValues.get(0).x;
		double firstVal = slope*firstTime+intercept;
		double lastTime = previousValues.get(previousValues.size() - 1).x;
		double lastVal = slope*lastTime+intercept;		
		FinePoint regresssionFirst = new FinePoint(firstTime, firstVal);
		FinePoint regresssionLast = new FinePoint(lastTime, lastVal);
		
		//add the points to the regression plot
		graphs.clearPlotPoints(rPlotName);
		graphs.addPoint(regresssionFirst, rPlotName);
		graphs.addPoint(regresssionLast, rPlotName);	
		rPlot.setLabel("r="+rstr, new FinePoint((lastTime+firstTime)/2, (lastVal+firstVal)/2));
	
		//determine if the point is an outlier.  if so, do not add it to the plot
		if(ySigma!=0){		
			NormalDistributionImpl d = new NormalDistributionImpl(yBar, ySigma);
			double pValue=0;
			try {
				pValue = d.cumulativeProbability(val);
			} catch (MathException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//System.out.println("mean: " + yBar + " sigma: " + ySigma + " pValue: " + pValue);
			if(pValue>=.1 && pValue <=.99){
				//add the point to the stream
				this.addPoint(val);
				return val;
			}else{
				//System.out.println("!!!Value of: " + val + " was rejected as an outlier for stream: " + this.name);
				//System.out.println("mean: " + yBar + " sigma: " + ySigma + " pValue: " + pValue);
				return null;
			}

		}else{
			//sigma is zero, add the point since it is clearly not an outlier
			this.addPoint(val);
			return val;
		}
		
	}
	
	public void setVisible(boolean visible) {
		this.visible = visible;
		graphs.setPlotVisibiltiy(rPlotName, visible);
	}

}
