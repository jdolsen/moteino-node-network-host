package dataStreams;

import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JPanel;

import com.digitalbotanist.javaPlots.MultiPlot;
import com.digitalbotanist.javaPlots.TimePlot;
import com.digitalbotanist.mnnHost.CommandField;
import com.digitalbotanist.mnnHost.MNN_Host;
import com.digitalbotanist.mnnHost.StatusPanelCreator;

public class DataProcessingHandler extends JPanel {

	public MNN_Host parent;
	StatusPanelCreator sPanel;
	private MultiPlot graphs;
	
	//links a list of incomming stream data to processed stream data
	HashMap<String, ArrayList<ProcessedStream>> streams = new HashMap<String, ArrayList<ProcessedStream>>();
	
	
	public DataProcessingHandler(MNN_Host parent, StatusPanelCreator sPanel, MultiPlot graphs){
		this.parent = parent;
		this.sPanel = sPanel;
		this.graphs = graphs;
		
	}
	
	public void updateDataItem(String name, Double val) {
		//get the list of streams for the item
		ArrayList<ProcessedStream> processors = streams.get(name);//nodeName+"-"+commandString+"-"+fieldname);
		if(processors!=null){
			for(ProcessedStream p:processors){
				Double retVal=p.addStreamValue(val);
				//if other processors are dependent on this one, update them too
				if(retVal!=null){
					updateDataItem(p.name,retVal);
				}
			}
		}
	}
	
	public void registerProcessedStream(String feedStream, ProcessedStream p){
		//pair the feedStream data to the processed stream object
		//it will get updated any time the feedStream gets new data
		if(streams.containsKey(p.name)){
			//names should be unique!  return
			System.out.println(p.name + " is not unique");
			return;
		}
		
		if(!streams.containsKey(feedStream)){
			streams.put(feedStream, new ArrayList<ProcessedStream>());
		}		
		streams.get(feedStream).add(p);
		//p.setPlotHolder(graphs);
		//create a new name for the processed stream?
		//display the processed stream on the multiplot
		parent.addProcessedStream(p.getName(),p);
	}

	public void init() {
		registerProcessedStream("iCoop1-eco-t",new OutlierProcessedStream(parent.GRAPHTIME, "iCoop1-eco-t-out5", graphs));
		//registerProcessedStream("iCoop1-eco-t",new InRangeProcessedStream(parent.GRAPHTIME, "iCoop1-eco-t-inRng",0.,100., graphs));
		//registerProcessedStream("iCoop1-eco-t",new AmountChangedProcessedStream(parent.GRAPHTIME, "iCoop1-eco-t-amt20",20., graphs));
		//registerProcessedStream("iCoop1-eco-t",new PercentChangedProcessedStream(parent.GRAPHTIME, "iCoop1-eco-t-amt20",20., graphs));
		//registerProcessedStream("iCoop1-eco-h",new windowAveragedProcessedStream(parent.GRAPHTIME, "iCoop1-eco-h-avg5"));
		//registerProcessedStream("iCoop1-eco-t-inRng",new OutlierProcessedStream(parent.GRAPHTIME, "iCoop1-eco-t-inRng-out5", graphs));

	}
	
	public void test(){
		
		try {
			updateDataItem("iCoop1-eco-t", 10.);
			Thread.sleep(1000);
			updateDataItem("iCoop1-eco-t", 10.);
			Thread.sleep(1000);
			updateDataItem("iCoop1-eco-t", 10.);
			Thread.sleep(1000);
			/*updateDataItem("iCoop1-eco-t", 10.);
			Thread.sleep(1000);
			updateDataItem("iCoop1-eco-t", 10.);
			Thread.sleep(1000);
			*/
			updateDataItem("iCoop1-eco-t", 60.);
			Thread.sleep(1000);
			updateDataItem("iCoop1-eco-t", 11.);
			Thread.sleep(1000);
			updateDataItem("iCoop1-eco-t", 12.);
			Thread.sleep(1000);
			updateDataItem("iCoop1-eco-t", 13.);
			Thread.sleep(1000);
			updateDataItem("iCoop1-eco-t", 14.);
			Thread.sleep(1000);
			
			updateDataItem("iCoop1-eco-t", 15.);
			Thread.sleep(1000);
			updateDataItem("iCoop1-eco-t", 15.);
			Thread.sleep(1000);
			updateDataItem("iCoop1-eco-t", 15.);
			Thread.sleep(1000);
			updateDataItem("iCoop1-eco-t", 15.);
			Thread.sleep(1000);
			updateDataItem("iCoop1-eco-t", 15.);
			Thread.sleep(1000);
			updateDataItem("iCoop1-eco-t", 15.);
			Thread.sleep(1000);
			updateDataItem("iCoop1-eco-t", 15.);
			Thread.sleep(1000);
			updateDataItem("iCoop1-eco-t", 15.);
			Thread.sleep(1000);
			updateDataItem("iCoop1-eco-t", 15.);
			Thread.sleep(1000);
			updateDataItem("iCoop1-eco-t", 15.);
			Thread.sleep(1000);
			updateDataItem("iCoop1-eco-t", 15.);
			Thread.sleep(1000);
			updateDataItem("iCoop1-eco-t", 15.);
			Thread.sleep(1000);
			updateDataItem("iCoop1-eco-t", 15.);
			Thread.sleep(1000);
			updateDataItem("iCoop1-eco-t", 15.);
			Thread.sleep(1000);
			updateDataItem("iCoop1-eco-t", 15.);
			Thread.sleep(1000);
			updateDataItem("iCoop1-eco-t", 15.);
			Thread.sleep(1000);
			updateDataItem("iCoop1-eco-t", 15.);
			Thread.sleep(1000);
			updateDataItem("iCoop1-eco-t", 15.);
			Thread.sleep(1000);
			updateDataItem("iCoop1-eco-t", 15.);
			Thread.sleep(1000);
			updateDataItem("iCoop1-eco-t", 15.);
			Thread.sleep(1000);
			updateDataItem("iCoop1-eco-t", 15.);
			Thread.sleep(1000);
			updateDataItem("iCoop1-eco-t", 150.);
			//updateDataItem("iCoop1-eco-t", 150.);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/*updateDataItem("iCoop1-eco-t", 10.);
		updateDataItem("iCoop1-eco-t", 10.);
		updateDataItem("iCoop1-eco-t", 10.);
		updateDataItem("iCoop1-eco-t", 10.);
		updateDataItem("iCoop1-eco-t", 50.);
		*/
	}
	
}
