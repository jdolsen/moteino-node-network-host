package dataStreams;

import com.digitalbotanist.javaPlots.MultiPlot;

public class PercentChangedProcessedStream extends ProcessedStream {

	private double maxPercentChange;
	private Double previousValue = null;

	public PercentChangedProcessedStream(int displaySeconds, String name, double maxPercentChange, MultiPlot graphs) {
		super(displaySeconds, name, graphs);
		this.maxPercentChange = maxPercentChange;
	}

	@Override
	public Double addStreamValue(double val) {
		if(previousValue==null){
			this.addPoint(val);
			previousValue = new Double(val);
			return val;
		}else{
			double diff = Math.abs(previousValue-val);
			double pct = (diff/previousValue)*100;
			
			if(pct>maxPercentChange){
				return null;
			}else{
				previousValue=val;
				this.addPoint(val);
				return val;
			}			
		}		
	}

}
